name := """e-griot"""
organization := "com.hackathon"

version := "1.0-SNAPSHOT"
javacOptions += "-Xlint:unchecked"
javacOptions += "-Xlint:deprecation"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies += guice

//routesGenerator := StaticRoutesGenerator

libraryDependencies ++= Seq(
  javaJdbc,
  javaWs,
  "org.postgresql" % "postgresql" % "42.1.4"
  ,"net.sf.jasperreports"  % "jasperreports" % "6.5.1"
  ,"net.sf.barcode4j" % "barcode4j" % "2.1"
  ,"org.apache.xmlgraphics" % "batik-bridge" % "1.9.1"
)

libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.8.1"

// https://mvnrepository.com/artifact/org.jooq/jooq
libraryDependencies += "org.jooq" % "jooq" % "3.11.9"

// https://mvnrepository.com/artifact/org.jooq/jooq-meta
libraryDependencies += "org.jooq" % "jooq-meta" % "3.11.9"

// https://mvnrepository.com/artifact/org.jooq/jooq-codegen
libraryDependencies += "org.jooq" % "jooq-codegen" % "3.11.9"


resolvers += "Jasper3rd" at "http://jaspersoft.artifactoryonline.com/jaspersoft/third-party-ce-artifacts/"
