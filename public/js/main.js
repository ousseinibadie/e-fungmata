//variables placées en global pour gestion par le module de test (selenium)
var autonumericGlobal=[];
var autonumericZeroGlobal=[];
var autonumericNegGlobal=[];
jQuery(function ($) {

  $("a").on("click", function(event){
    if ($(this).is("[disabled]")) {
      event.preventDefault();
    }
  });

  $(function () {
    /*
     * Initialise print preview plugin
     */
    // Add link for print preview and intialise
    //$('#print-button').prepend('<a class="print-preview btn btn-primary">Imprimer</a>');
    $('a.print-preview').printPreview();
  });
  /*
    $(function(){
      window.location.replace('#');
      //return false;
    });
    */
  /*autonumeric*/

  if ($(".autonumeric").length > 0) { //instantiate only if elements exist in the context on the page loaded
    var autoNumericOptions = {
      currencySymbol: '',
      digitGroupSeparator: ' ',
      minimumValue: '1',
      unformatOnSubmit: true

    };
    autoNumericOptions = $.extend({}, AutoNumeric.getPredefinedOptions().integerPos, autoNumericOptions);
    autonumericGlobal = AutoNumeric.multiple('.autonumeric', autoNumericOptions);
    $(".autonumeric").css('text-align', 'right');
  }

  if ($(".autonumeric-zero").length > 0) { //instantiate only if elements exist in the context on the page loaded
    var autoNumericZeroOptions = {
      currencySymbol: '',
      digitGroupSeparator: ' ',
      //minimumValue: '1',
      unformatOnSubmit: true

    };
    autoNumericZeroOptions = $.extend({}, AutoNumeric.getPredefinedOptions().integerPos, autoNumericZeroOptions);
    autonumericZeroGlobal = AutoNumeric.multiple('.autonumeric-zero', autoNumericZeroOptions);
    $(".autonumeric-zero").css('text-align', 'right');
  }

  if ($(".autonumeric-negative").length > 0) { //instantiate only if elements exist in the context on the page loaded
	    var autoNumericNegOptions = {
	      currencySymbol: '',
	      digitGroupSeparator: ' ',
	      minimumValue: '-99999999999999999999999999',
	      unformatOnSubmit: true

	    };
	    autoNumericNegOptions = $.extend({}, AutoNumeric.getPredefinedOptions().integerPos, autoNumericNegOptions);
	    autonumericNegGlobal = AutoNumeric.multiple('.autonumeric-negative', autoNumericNegOptions);
	    $(".autonumeric-negative").css('text-align', 'right');
	  }

  /*datepicker*/
  $('.datepicker').datepicker({
    language: 'fr',
    format: 'dd/mm/yyyy'
  });

  /*compte*/
  $('#codeDepense').change(function () {
    /*initialisation du champ*/
    $('#div_virtualDepense').val("..");
    var codeDepense = $('#codeDepense').val();
    if (codeDepense) {
      $.getJSON('/engagement/data/depense/' + codeDepense, function (data) {
        $('#div_virtualDepense').val(data.codeDepense + "--" + data.libelleCodeDepense);
      });
    }
  });

  $('#activites_service_engagement').change(function () {
      var codeChapire = "" + $(this).find('option:selected').val();
      top.document.location = "/engagement-attente/activites/service/" + codeChapire;
    });

    $('#activites_service_delegation').change(function () {
        var codeChapire = "" + $(this).find('option:selected').val();
        top.document.location = "/delegation-attente/activites/service/" + codeChapire;
      });

  /*localité*/
  $('#codeLocalite').change(function () {
    $('#designation').val("...");

    /* setting currently changed option value to option variable */
    var codeLocalite = $('#codeLocalite').val();
    //var option = $(this).find('option:selected').val();
    //alert(option);
    /* setting input box value to selected option value */
    /*json value*/
    $.getJSON('/localite/' + codeLocalite, function (data) {
      $.each(data, function (name, value) {
        $('#designation').val(value);
      });

    });

  });


  $(".categorieCheck").change(function () {
    var section = $('#sectionCodeForJs').val();
    var programme = $('#programmeCodeForJs').val();
    var categorie = $(this).val();
    $('.imputationJs').text(section + "" + programme + "" + categorie);
    $('.imputationJs').val(section + "" + programme + "" + categorie);
  });


  /*-----------------------Select all chekbox----------------*/
  $('#globalCheckbox').click(function () {
    if ($(this).prop("checked")) {
      $(".select_class").prop("checked", true);
    } else {
      $(".select_class").prop("checked", false);
    }
  });


  /*--------------------- Selection les engagements*/
  $('button[id=button]').click(function () {
    var values = [];
    $('.select_class:checked').each(function () {
      values.push($(this).val());
    });
    $('#valSelected').val(values);
  });


  $('.montantSectoriel').number(true, 0, ',', ' ');

  /*----------------------- Localite et service bénéficiaire------------------------------*/
    $(".geographieCheck").change(function () {
  	  	$('#localite').empty();
  	  	$('#service').empty();
  	    var section = $('#sectionCodeForJs').val();
  	    var geographie = $(this).val();
  	     $('#geographie-key').val(geographie);
  	    $('#localite').append($('<option>', {
  	        value: "",
  	        text : "-- Choisir la localité ----"
  	    }));

  	    $('#service').append($('<option>', {
  	        value: "",
  	        text : "-- Choisir le service bénéficiaire ----"
  	    }));

  	    $.getJSON('/localite/'+geographie, function(data){

  			   $.each(data, function(name, value) {
  				   $('#localite').append($('<option>', {
  				        value: value.code,
  				        text : value.code+ " - " + value.intitule
  				    }));
  			   });
  	    });

  	    $.getJSON('/section/'+section+'/geo/'+geographie, function(data){
  			   $.each(data, function(name, value) {
  				   $('#service').append($('<option>', {
  				        value: value.code,
  				        text : value.code+ " - " + value.intitule
  				    }));

  			   });
  	    });
    });

});
