/*
 * This file is generated by jOOQ.
 */
package models.tables.daos;


import java.time.LocalDateTime;
import java.util.List;

import models.tables.IndicateurProjet;
import models.tables.records.IndicateurProjetRecord;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class IndicateurProjetDao extends DAOImpl<IndicateurProjetRecord, models.tables.pojos.IndicateurProjet, Long> {

    /**
     * Create a new IndicateurProjetDao without any configuration
     */
    public IndicateurProjetDao() {
        super(IndicateurProjet.INDICATEUR_PROJET, models.tables.pojos.IndicateurProjet.class);
    }

    /**
     * Create a new IndicateurProjetDao with an attached configuration
     */
    public IndicateurProjetDao(Configuration configuration) {
        super(IndicateurProjet.INDICATEUR_PROJET, models.tables.pojos.IndicateurProjet.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Long getId(models.tables.pojos.IndicateurProjet object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchById(Long... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public models.tables.pojos.IndicateurProjet fetchOneById(Long value) {
        return fetchOne(IndicateurProjet.INDICATEUR_PROJET.ID, value);
    }

    /**
     * Fetch records that have <code>code IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByCode(String... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.CODE, values);
    }

    /**
     * Fetch records that have <code>hypothese IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByHypothese(String... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.HYPOTHESE, values);
    }

    /**
     * Fetch records that have <code>indicateur IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByIndicateur(String... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.INDICATEUR, values);
    }

    /**
     * Fetch records that have <code>moyen_de_verification IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByMoyenDeVerification(String... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.MOYEN_DE_VERIFICATION, values);
    }

    /**
     * Fetch records that have <code>resultat IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByResultat(String... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.RESULTAT, values);
    }

    /**
     * Fetch records that have <code>projet_id IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByProjetId(Long... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.PROJET_ID, values);
    }

    /**
     * Fetch records that have <code>when_done IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByWhenDone(LocalDateTime... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.WHEN_DONE, values);
    }

    /**
     * Fetch records that have <code>who_done IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByWhoDone(String... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.WHO_DONE, values);
    }

    /**
     * Fetch records that have <code>last_update IN (values)</code>
     */
    public List<models.tables.pojos.IndicateurProjet> fetchByLastUpdate(LocalDateTime... values) {
        return fetch(IndicateurProjet.INDICATEUR_PROJET.LAST_UPDATE, values);
    }
}
