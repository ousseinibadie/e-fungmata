/*
 * This file is generated by jOOQ.
 */
package models.tables.daos;


import java.time.LocalDateTime;
import java.util.List;

import models.tables.ProjetValide;
import models.tables.records.ProjetValideRecord;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProjetValideDao extends DAOImpl<ProjetValideRecord, models.tables.pojos.ProjetValide, Long> {

    /**
     * Create a new ProjetValideDao without any configuration
     */
    public ProjetValideDao() {
        super(ProjetValide.PROJET_VALIDE, models.tables.pojos.ProjetValide.class);
    }

    /**
     * Create a new ProjetValideDao with an attached configuration
     */
    public ProjetValideDao(Configuration configuration) {
        super(ProjetValide.PROJET_VALIDE, models.tables.pojos.ProjetValide.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Long getId(models.tables.pojos.ProjetValide object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchById(Long... values) {
        return fetch(ProjetValide.PROJET_VALIDE.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public models.tables.pojos.ProjetValide fetchOneById(Long value) {
        return fetchOne(ProjetValide.PROJET_VALIDE.ID, value);
    }

    /**
     * Fetch records that have <code>nom IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchByNom(String... values) {
        return fetch(ProjetValide.PROJET_VALIDE.NOM, values);
    }

    /**
     * Fetch records that have <code>description IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchByDescription(String... values) {
        return fetch(ProjetValide.PROJET_VALIDE.DESCRIPTION, values);
    }

    /**
     * Fetch records that have <code>type_projet IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchByTypeProjet(String... values) {
        return fetch(ProjetValide.PROJET_VALIDE.TYPE_PROJET, values);
    }

    /**
     * Fetch records that have <code>secteur_activite IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchBySecteurActivite(String... values) {
        return fetch(ProjetValide.PROJET_VALIDE.SECTEUR_ACTIVITE, values);
    }

    /**
     * Fetch records that have <code>document_fournit IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchByDocumentFournit(String... values) {
        return fetch(ProjetValide.PROJET_VALIDE.DOCUMENT_FOURNIT, values);
    }

    /**
     * Fetch records that have <code>montant IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchByMontant(Long... values) {
        return fetch(ProjetValide.PROJET_VALIDE.MONTANT, values);
    }

    /**
     * Fetch records that have <code>satuts IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchBySatuts(String... values) {
        return fetch(ProjetValide.PROJET_VALIDE.SATUTS, values);
    }

    /**
     * Fetch records that have <code>who_done IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchByWhoDone(String... values) {
        return fetch(ProjetValide.PROJET_VALIDE.WHO_DONE, values);
    }

    /**
     * Fetch records that have <code>when_done IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchByWhenDone(LocalDateTime... values) {
        return fetch(ProjetValide.PROJET_VALIDE.WHEN_DONE, values);
    }

    /**
     * Fetch records that have <code>last_update IN (values)</code>
     */
    public List<models.tables.pojos.ProjetValide> fetchByLastUpdate(LocalDateTime... values) {
        return fetch(ProjetValide.PROJET_VALIDE.LAST_UPDATE, values);
    }
}
