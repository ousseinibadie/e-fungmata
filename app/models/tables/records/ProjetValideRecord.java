/*
 * This file is generated by jOOQ.
 */
package models.tables.records;


import java.time.LocalDateTime;

import models.tables.ProjetValide;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record11;
import org.jooq.Row11;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProjetValideRecord extends UpdatableRecordImpl<ProjetValideRecord> implements Record11<Long, String, String, String, String, String, Long, String, String, LocalDateTime, LocalDateTime> {

    private static final long serialVersionUID = 1989493916;

    /**
     * Setter for <code>projet.projet_valide.id</code>.
     */
    public ProjetValideRecord setId(Long value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>projet.projet_valide.nom</code>.
     */
    public ProjetValideRecord setNom(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.nom</code>.
     */
    public String getNom() {
        return (String) get(1);
    }

    /**
     * Setter for <code>projet.projet_valide.description</code>.
     */
    public ProjetValideRecord setDescription(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.description</code>.
     */
    public String getDescription() {
        return (String) get(2);
    }

    /**
     * Setter for <code>projet.projet_valide.type_projet</code>.
     */
    public ProjetValideRecord setTypeProjet(String value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.type_projet</code>.
     */
    public String getTypeProjet() {
        return (String) get(3);
    }

    /**
     * Setter for <code>projet.projet_valide.secteur_activite</code>.
     */
    public ProjetValideRecord setSecteurActivite(String value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.secteur_activite</code>.
     */
    public String getSecteurActivite() {
        return (String) get(4);
    }

    /**
     * Setter for <code>projet.projet_valide.document_fournit</code>.
     */
    public ProjetValideRecord setDocumentFournit(String value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.document_fournit</code>.
     */
    public String getDocumentFournit() {
        return (String) get(5);
    }

    /**
     * Setter for <code>projet.projet_valide.montant</code>.
     */
    public ProjetValideRecord setMontant(Long value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.montant</code>.
     */
    public Long getMontant() {
        return (Long) get(6);
    }

    /**
     * Setter for <code>projet.projet_valide.satuts</code>.
     */
    public ProjetValideRecord setSatuts(String value) {
        set(7, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.satuts</code>.
     */
    public String getSatuts() {
        return (String) get(7);
    }

    /**
     * Setter for <code>projet.projet_valide.who_done</code>.
     */
    public ProjetValideRecord setWhoDone(String value) {
        set(8, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.who_done</code>.
     */
    public String getWhoDone() {
        return (String) get(8);
    }

    /**
     * Setter for <code>projet.projet_valide.when_done</code>.
     */
    public ProjetValideRecord setWhenDone(LocalDateTime value) {
        set(9, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.when_done</code>.
     */
    public LocalDateTime getWhenDone() {
        return (LocalDateTime) get(9);
    }

    /**
     * Setter for <code>projet.projet_valide.last_update</code>.
     */
    public ProjetValideRecord setLastUpdate(LocalDateTime value) {
        set(10, value);
        return this;
    }

    /**
     * Getter for <code>projet.projet_valide.last_update</code>.
     */
    public LocalDateTime getLastUpdate() {
        return (LocalDateTime) get(10);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record11 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row11<Long, String, String, String, String, String, Long, String, String, LocalDateTime, LocalDateTime> fieldsRow() {
        return (Row11) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row11<Long, String, String, String, String, String, Long, String, String, LocalDateTime, LocalDateTime> valuesRow() {
        return (Row11) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return ProjetValide.PROJET_VALIDE.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return ProjetValide.PROJET_VALIDE.NOM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return ProjetValide.PROJET_VALIDE.DESCRIPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return ProjetValide.PROJET_VALIDE.TYPE_PROJET;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return ProjetValide.PROJET_VALIDE.SECTEUR_ACTIVITE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field6() {
        return ProjetValide.PROJET_VALIDE.DOCUMENT_FOURNIT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field7() {
        return ProjetValide.PROJET_VALIDE.MONTANT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field8() {
        return ProjetValide.PROJET_VALIDE.SATUTS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field9() {
        return ProjetValide.PROJET_VALIDE.WHO_DONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<LocalDateTime> field10() {
        return ProjetValide.PROJET_VALIDE.WHEN_DONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<LocalDateTime> field11() {
        return ProjetValide.PROJET_VALIDE.LAST_UPDATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getNom();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getTypeProjet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component5() {
        return getSecteurActivite();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component6() {
        return getDocumentFournit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component7() {
        return getMontant();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component8() {
        return getSatuts();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component9() {
        return getWhoDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime component10() {
        return getWhenDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime component11() {
        return getLastUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getNom();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getTypeProjet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getSecteurActivite();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value6() {
        return getDocumentFournit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value7() {
        return getMontant();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value8() {
        return getSatuts();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value9() {
        return getWhoDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime value10() {
        return getWhenDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime value11() {
        return getLastUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value1(Long value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value2(String value) {
        setNom(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value3(String value) {
        setDescription(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value4(String value) {
        setTypeProjet(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value5(String value) {
        setSecteurActivite(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value6(String value) {
        setDocumentFournit(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value7(Long value) {
        setMontant(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value8(String value) {
        setSatuts(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value9(String value) {
        setWhoDone(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value10(LocalDateTime value) {
        setWhenDone(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord value11(LocalDateTime value) {
        setLastUpdate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjetValideRecord values(Long value1, String value2, String value3, String value4, String value5, String value6, Long value7, String value8, String value9, LocalDateTime value10, LocalDateTime value11) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ProjetValideRecord
     */
    public ProjetValideRecord() {
        super(ProjetValide.PROJET_VALIDE);
    }

    /**
     * Create a detached, initialised ProjetValideRecord
     */
    public ProjetValideRecord(Long id, String nom, String description, String typeProjet, String secteurActivite, String documentFournit, Long montant, String satuts, String whoDone, LocalDateTime whenDone, LocalDateTime lastUpdate) {
        super(ProjetValide.PROJET_VALIDE);

        set(0, id);
        set(1, nom);
        set(2, description);
        set(3, typeProjet);
        set(4, secteurActivite);
        set(5, documentFournit);
        set(6, montant);
        set(7, satuts);
        set(8, whoDone);
        set(9, whenDone);
        set(10, lastUpdate);
    }
}
