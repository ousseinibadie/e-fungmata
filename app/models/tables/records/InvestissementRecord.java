/*
 * This file is generated by jOOQ.
 */
package models.tables.records;


import java.time.LocalDateTime;

import models.tables.Investissement;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class InvestissementRecord extends UpdatableRecordImpl<InvestissementRecord> implements Record7<Long, Long, Long, Long, LocalDateTime, String, LocalDateTime> {

    private static final long serialVersionUID = -1686675852;

    /**
     * Setter for <code>projet.investissement.id</code>.
     */
    public InvestissementRecord setId(Long value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>projet.investissement.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>projet.investissement.projet_id</code>.
     */
    public InvestissementRecord setProjetId(Long value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>projet.investissement.projet_id</code>.
     */
    public Long getProjetId() {
        return (Long) get(1);
    }

    /**
     * Setter for <code>projet.investissement.investisseur</code>.
     */
    public InvestissementRecord setInvestisseur(Long value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>projet.investissement.investisseur</code>.
     */
    public Long getInvestisseur() {
        return (Long) get(2);
    }

    /**
     * Setter for <code>projet.investissement.montant</code>.
     */
    public InvestissementRecord setMontant(Long value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>projet.investissement.montant</code>.
     */
    public Long getMontant() {
        return (Long) get(3);
    }

    /**
     * Setter for <code>projet.investissement.when_done</code>.
     */
    public InvestissementRecord setWhenDone(LocalDateTime value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>projet.investissement.when_done</code>.
     */
    public LocalDateTime getWhenDone() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>projet.investissement.who_done</code>.
     */
    public InvestissementRecord setWhoDone(String value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>projet.investissement.who_done</code>.
     */
    public String getWhoDone() {
        return (String) get(5);
    }

    /**
     * Setter for <code>projet.investissement.last_update</code>.
     */
    public InvestissementRecord setLastUpdate(LocalDateTime value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>projet.investissement.last_update</code>.
     */
    public LocalDateTime getLastUpdate() {
        return (LocalDateTime) get(6);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record7 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row7<Long, Long, Long, Long, LocalDateTime, String, LocalDateTime> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row7<Long, Long, Long, Long, LocalDateTime, String, LocalDateTime> valuesRow() {
        return (Row7) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return Investissement.INVESTISSEMENT.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field2() {
        return Investissement.INVESTISSEMENT.PROJET_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field3() {
        return Investissement.INVESTISSEMENT.INVESTISSEUR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field4() {
        return Investissement.INVESTISSEMENT.MONTANT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<LocalDateTime> field5() {
        return Investissement.INVESTISSEMENT.WHEN_DONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field6() {
        return Investissement.INVESTISSEMENT.WHO_DONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<LocalDateTime> field7() {
        return Investissement.INVESTISSEMENT.LAST_UPDATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component2() {
        return getProjetId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component3() {
        return getInvestisseur();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component4() {
        return getMontant();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime component5() {
        return getWhenDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component6() {
        return getWhoDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime component7() {
        return getLastUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value2() {
        return getProjetId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value3() {
        return getInvestisseur();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value4() {
        return getMontant();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime value5() {
        return getWhenDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value6() {
        return getWhoDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime value7() {
        return getLastUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvestissementRecord value1(Long value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvestissementRecord value2(Long value) {
        setProjetId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvestissementRecord value3(Long value) {
        setInvestisseur(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvestissementRecord value4(Long value) {
        setMontant(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvestissementRecord value5(LocalDateTime value) {
        setWhenDone(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvestissementRecord value6(String value) {
        setWhoDone(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvestissementRecord value7(LocalDateTime value) {
        setLastUpdate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvestissementRecord values(Long value1, Long value2, Long value3, Long value4, LocalDateTime value5, String value6, LocalDateTime value7) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached InvestissementRecord
     */
    public InvestissementRecord() {
        super(Investissement.INVESTISSEMENT);
    }

    /**
     * Create a detached, initialised InvestissementRecord
     */
    public InvestissementRecord(Long id, Long projetId, Long investisseur, Long montant, LocalDateTime whenDone, String whoDone, LocalDateTime lastUpdate) {
        super(Investissement.INVESTISSEMENT);

        set(0, id);
        set(1, projetId);
        set(2, investisseur);
        set(3, montant);
        set(4, whenDone);
        set(5, whoDone);
        set(6, lastUpdate);
    }
}
