/*
 * This file is generated by jOOQ.
 */
package models.tables.records;


import java.time.LocalDateTime;

import models.tables.ApplicationUserProfil;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ApplicationUserProfilRecord extends UpdatableRecordImpl<ApplicationUserProfilRecord> implements Record6<Long, String, String, String, LocalDateTime, LocalDateTime> {

    private static final long serialVersionUID = -1655435352;

    /**
     * Setter for <code>projet.application_user_profil.id</code>.
     */
    public ApplicationUserProfilRecord setId(Long value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>projet.application_user_profil.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>projet.application_user_profil.email</code>.
     */
    public ApplicationUserProfilRecord setEmail(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>projet.application_user_profil.email</code>.
     */
    public String getEmail() {
        return (String) get(1);
    }

    /**
     * Setter for <code>projet.application_user_profil.profil</code>.
     */
    public ApplicationUserProfilRecord setProfil(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>projet.application_user_profil.profil</code>.
     */
    public String getProfil() {
        return (String) get(2);
    }

    /**
     * Setter for <code>projet.application_user_profil.who_done</code>.
     */
    public ApplicationUserProfilRecord setWhoDone(String value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>projet.application_user_profil.who_done</code>.
     */
    public String getWhoDone() {
        return (String) get(3);
    }

    /**
     * Setter for <code>projet.application_user_profil.when_done</code>.
     */
    public ApplicationUserProfilRecord setWhenDone(LocalDateTime value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>projet.application_user_profil.when_done</code>.
     */
    public LocalDateTime getWhenDone() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>projet.application_user_profil.last_update</code>.
     */
    public ApplicationUserProfilRecord setLastUpdate(LocalDateTime value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>projet.application_user_profil.last_update</code>.
     */
    public LocalDateTime getLastUpdate() {
        return (LocalDateTime) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Long, String, String, String, LocalDateTime, LocalDateTime> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Long, String, String, String, LocalDateTime, LocalDateTime> valuesRow() {
        return (Row6) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return ApplicationUserProfil.APPLICATION_USER_PROFIL.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return ApplicationUserProfil.APPLICATION_USER_PROFIL.EMAIL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return ApplicationUserProfil.APPLICATION_USER_PROFIL.PROFIL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return ApplicationUserProfil.APPLICATION_USER_PROFIL.WHO_DONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<LocalDateTime> field5() {
        return ApplicationUserProfil.APPLICATION_USER_PROFIL.WHEN_DONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<LocalDateTime> field6() {
        return ApplicationUserProfil.APPLICATION_USER_PROFIL.LAST_UPDATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getEmail();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getProfil();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getWhoDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime component5() {
        return getWhenDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime component6() {
        return getLastUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getEmail();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getProfil();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getWhoDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime value5() {
        return getWhenDone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime value6() {
        return getLastUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationUserProfilRecord value1(Long value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationUserProfilRecord value2(String value) {
        setEmail(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationUserProfilRecord value3(String value) {
        setProfil(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationUserProfilRecord value4(String value) {
        setWhoDone(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationUserProfilRecord value5(LocalDateTime value) {
        setWhenDone(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationUserProfilRecord value6(LocalDateTime value) {
        setLastUpdate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationUserProfilRecord values(Long value1, String value2, String value3, String value4, LocalDateTime value5, LocalDateTime value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ApplicationUserProfilRecord
     */
    public ApplicationUserProfilRecord() {
        super(ApplicationUserProfil.APPLICATION_USER_PROFIL);
    }

    /**
     * Create a detached, initialised ApplicationUserProfilRecord
     */
    public ApplicationUserProfilRecord(Long id, String email, String profil, String whoDone, LocalDateTime whenDone, LocalDateTime lastUpdate) {
        super(ApplicationUserProfil.APPLICATION_USER_PROFIL);

        set(0, id);
        set(1, email);
        set(2, profil);
        set(3, whoDone);
        set(4, whenDone);
        set(5, lastUpdate);
    }
}
