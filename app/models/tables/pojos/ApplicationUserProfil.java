/*
 * This file is generated by jOOQ.
 */
package models.tables.pojos;


import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ApplicationUserProfil implements Serializable {

    private static final long serialVersionUID = 848676277;

    private Long          id;
    private String        email;
    private String        profil;
    private String        whoDone;
    private LocalDateTime whenDone;
    private LocalDateTime lastUpdate;

    public ApplicationUserProfil() {}

    public ApplicationUserProfil(ApplicationUserProfil value) {
        this.id = value.id;
        this.email = value.email;
        this.profil = value.profil;
        this.whoDone = value.whoDone;
        this.whenDone = value.whenDone;
        this.lastUpdate = value.lastUpdate;
    }

    public ApplicationUserProfil(
        Long          id,
        String        email,
        String        profil,
        String        whoDone,
        LocalDateTime whenDone,
        LocalDateTime lastUpdate
    ) {
        this.id = id;
        this.email = email;
        this.profil = profil;
        this.whoDone = whoDone;
        this.whenDone = whenDone;
        this.lastUpdate = lastUpdate;
    }

    public Long getId() {
        return this.id;
    }

    public ApplicationUserProfil setId(Long id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return this.email;
    }

    public ApplicationUserProfil setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getProfil() {
        return this.profil;
    }

    public ApplicationUserProfil setProfil(String profil) {
        this.profil = profil;
        return this;
    }

    public String getWhoDone() {
        return this.whoDone;
    }

    public ApplicationUserProfil setWhoDone(String whoDone) {
        this.whoDone = whoDone;
        return this;
    }

    public LocalDateTime getWhenDone() {
        return this.whenDone;
    }

    public ApplicationUserProfil setWhenDone(LocalDateTime whenDone) {
        this.whenDone = whenDone;
        return this;
    }

    public LocalDateTime getLastUpdate() {
        return this.lastUpdate;
    }

    public ApplicationUserProfil setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ApplicationUserProfil other = (ApplicationUserProfil) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (email == null) {
            if (other.email != null)
                return false;
        }
        else if (!email.equals(other.email))
            return false;
        if (profil == null) {
            if (other.profil != null)
                return false;
        }
        else if (!profil.equals(other.profil))
            return false;
        if (whoDone == null) {
            if (other.whoDone != null)
                return false;
        }
        else if (!whoDone.equals(other.whoDone))
            return false;
        if (whenDone == null) {
            if (other.whenDone != null)
                return false;
        }
        else if (!whenDone.equals(other.whenDone))
            return false;
        if (lastUpdate == null) {
            if (other.lastUpdate != null)
                return false;
        }
        else if (!lastUpdate.equals(other.lastUpdate))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = prime * result + ((this.email == null) ? 0 : this.email.hashCode());
        result = prime * result + ((this.profil == null) ? 0 : this.profil.hashCode());
        result = prime * result + ((this.whoDone == null) ? 0 : this.whoDone.hashCode());
        result = prime * result + ((this.whenDone == null) ? 0 : this.whenDone.hashCode());
        result = prime * result + ((this.lastUpdate == null) ? 0 : this.lastUpdate.hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ApplicationUserProfil (");

        sb.append(id);
        sb.append(", ").append(email);
        sb.append(", ").append(profil);
        sb.append(", ").append(whoDone);
        sb.append(", ").append(whenDone);
        sb.append(", ").append(lastUpdate);

        sb.append(")");
        return sb.toString();
    }
}
