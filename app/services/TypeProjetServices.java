package services;

import com.google.inject.Inject;
import commons.Pagination;
import models.Tables;

import models.tables.daos.TypeProjetDao;
import models.tables.pojos.TypeProjet;
import modules.IConnectionHelper;
import org.jooq.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TypeProjetServices {

    private final IConnectionHelper con;
    private final Configuration conf;

    public TypeProjetDao typeProjetDao;

    @Inject
    public TypeProjetServices(IConnectionHelper con){
        this.con = con;
        this.conf = this.con.connection().configuration();

        this.typeProjetDao = new TypeProjetDao(conf);

    }

    /**
     *
     * @return  Collection Map avec key=code et value=intittule du type de projet
     */
    public Map<String, String> options() {
        Map<String, String> option = new HashMap<String, String>();
        for (TypeProjet typeProjet : typeProjetDao.findAll()) {
            option.put(typeProjet.getCode(), typeProjet.getIntitule());
        }
        return option;
    }


    /**
     *
     * @param page
     * @param pageSize
     * @param filter
     * @return Liste paginée des types de projets
     */
    public Pagination<TypeProjet> page(int page, int pageSize, String filter) {

        if(page < 1) page = 1;
        Long total =  con.connection().selectCount().from(Tables.TYPE_PROJET).where(Tables.TYPE_PROJET.CODE.like('%'+filter+'%').or(Tables.TYPE_PROJET.INTITULE.like('%'+filter+'%'))).fetchAny(0, Long.class);
        List<TypeProjet> data = con.connection().selectFrom(Tables.TYPE_PROJET).where(Tables.TYPE_PROJET.CODE.like('%'+filter+'%').or(Tables.TYPE_PROJET.INTITULE.like('%'+filter+'%'))).offset((page-1)*pageSize).limit(pageSize).fetchInto(TypeProjet.class);

        return new Pagination<>(data, total, page, pageSize);
    }



}
