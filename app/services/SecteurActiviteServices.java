package services;

import com.google.inject.Inject;
import commons.Pagination;
import models.Tables;
import models.tables.daos.SecteurActiviteDao;
import models.tables.pojos.SecteurActivite;
import modules.IConnectionHelper;
import org.jooq.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecteurActiviteServices {

    private final IConnectionHelper con;
    private final Configuration conf;

    public SecteurActiviteDao typeProjetDao;

    @Inject
    public SecteurActiviteServices(IConnectionHelper con){
        this.con = con;
        this.conf = this.con.connection().configuration();

        this.typeProjetDao = new SecteurActiviteDao(conf);

    }

    /**
     *
     * @return  Collection Map avec key=code et value=intittule du type de financement
     */
    public Map<String, String> options() {
        Map<String, String> option = new HashMap<String, String>();
        for (SecteurActivite typeFinancement : typeProjetDao.findAll()) {
            option.put(typeFinancement.getCode(), typeFinancement.getIntitule());
        }
        return option;
    }


    /**
     *
     * @param page
     * @param pageSize
     * @param filter
     * @return Liste paginée des secteurs d'activité
     */
    public Pagination<SecteurActivite> page(int page, int pageSize, String filter) {

        if(page < 1) page = 1;
        Long total =  con.connection().selectCount().from(Tables.SECTEUR_ACTIVITE).where(Tables.SECTEUR_ACTIVITE.CODE.like('%'+filter+'%').or(Tables.SECTEUR_ACTIVITE.INTITULE.like('%'+filter+'%'))).fetchAny(0, Long.class);
        List<SecteurActivite> data = con.connection().selectFrom(Tables.SECTEUR_ACTIVITE).where(Tables.SECTEUR_ACTIVITE.CODE.like('%'+filter+'%').or(Tables.SECTEUR_ACTIVITE.INTITULE.like('%'+filter+'%'))).offset((page-1)*pageSize).limit(pageSize).fetchInto(SecteurActivite.class);

        return new Pagination<>(data, total, page, pageSize);
    }

}
