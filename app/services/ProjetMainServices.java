package services;

import com.google.inject.Inject;
import commons.Pagination;
import models.Tables;
import models.tables.daos.InvestissementDao;
import models.tables.daos.ProjetAtttenteDao;
import models.tables.daos.ProjetValideDao;
import models.tables.pojos.ProjetAtttente;
import models.tables.pojos.ProjetValide;
import models.tables.pojos.VProjetAttente;
import modules.IConnectionHelper;
import org.jooq.Configuration;

import java.util.List;

import static models.tables.VProjetAttente.V_PROJET_ATTENTE;

public class ProjetMainServices {

    private final IConnectionHelper con;
    private final Configuration conf;

    public ProjetAtttenteDao projetAtttenteDao;
    public InvestissementDao investissementDao;

    public ProjetValideDao projetDao;

    @Inject
    public ProjetMainServices(IConnectionHelper con){
        this.con = con;
        this.conf = this.con.connection().configuration();

        this.projetAtttenteDao = new ProjetAtttenteDao(conf);
        this.projetDao = new ProjetValideDao(conf);
        this.investissementDao = new InvestissementDao(conf);

    }


    /**
     *
     * @param page
     * @param pageSize
     * @param filter
     * @return Liste paginée des projet en attente de financement
     */
    public Pagination<ProjetAtttente> pageProjetAttente(int page, int pageSize, String filter) {

        if(page < 1) page = 1;
        Long total =  con.connection().selectCount().from(Tables.PROJET_ATTTENTE)
                .where(Tables.PROJET_ATTTENTE.NOM.like('%'+filter+'%')
                        .or(Tables.PROJET_ATTTENTE.DESCRIPTION
                                .like('%'+filter+'%'))).fetchAny(0, Long.class);

        List<ProjetAtttente> data = con.connection().selectFrom(Tables.PROJET_ATTTENTE)
                .where(Tables.PROJET_ATTTENTE.NOM
                        .like('%'+filter+'%')
                        .or(Tables.PROJET_ATTTENTE.DESCRIPTION
                                .like('%'+filter+'%')))
                .offset((page-1)*pageSize).limit(pageSize)
                .fetchInto(ProjetAtttente.class);

        return new Pagination<>(data, total, page, pageSize);
    }


    /**
     *
     * @param page
     * @param pageSize
     * @param filter
     * @return Liste paginée des projet en attente de financement
     */
    public Pagination<ProjetValide> pageProjetValide(int page, int pageSize, String filter) {

        if(page < 1) page = 1;
        Long total =  con.connection().selectCount().from(Tables.PROJET_VALIDE).where(Tables.PROJET_VALIDE.NOM.like('%'+filter+'%').or(Tables.PROJET_VALIDE.DESCRIPTION.like('%'+filter+'%'))).fetchAny(0, Long.class);
        List<ProjetValide> data = con.connection().selectFrom(Tables.PROJET_VALIDE).where(Tables.PROJET_VALIDE.NOM.like('%'+filter+'%').or(Tables.PROJET_VALIDE.DESCRIPTION.like('%'+filter+'%'))).offset((page-1)*pageSize).limit(pageSize).fetchInto(ProjetValide.class);

        return new Pagination<>(data, total, page, pageSize);
    }


    /**
     *
     * @return
     */
    public List<VProjetAttente> vProjetAttenteFindAll(){
       return con.connection().selectFrom(V_PROJET_ATTENTE).fetchInto(VProjetAttente.class);
    }
}
