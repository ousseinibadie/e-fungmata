package services;

import commons.BCryptHash;
import commons.Pagination;
import models.tables.daos.ApplicationProfilDao;
import models.tables.daos.ApplicationUserDao;
import models.tables.daos.ApplicationUserProfilDao;
import models.tables.pojos.ApplicationUser;
import modules.IConnectionHelper;
import org.jooq.Configuration;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static models.tables.ApplicationProfil.APPLICATION_PROFIL;
import static models.tables.ApplicationUser.APPLICATION_USER;

/**
 * @author abdoul-razack
 * @company Advinteck
 * @since 20181003
 * @version 1.0
 */
public class ApplicationMainServices {

    private static final String OK = "OK";
    private final IConnectionHelper con;
    private final Configuration conf;

    public ApplicationProfilDao applicationProfilDao;
    public ApplicationUserDao applicationUserDao;
    public ApplicationUserProfilDao applicationUserProfilDao;

    @Inject
    public ApplicationMainServices(IConnectionHelper con) {
        this.con = con;
        this.conf = this.con.connection().configuration();

        this.applicationProfilDao = new ApplicationProfilDao(conf);
        this.applicationUserDao = new ApplicationUserDao(conf);
        this.applicationUserProfilDao = new ApplicationUserProfilDao(conf);

    }


    public String delete(Long id) {
        try{
            con.connection().deleteFrom(APPLICATION_USER).where(APPLICATION_USER.ID.eq(id)).execute();
            return "OK";
        }catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }

    }


    public ApplicationUser authentification(String email, String password) {
        ApplicationUser applicationUser = new ApplicationUser();
        try {
            if(BCryptHash.checkPassword(password, applicationUserDao.fetchOneByEmail(email).getPassword()))
                password = applicationUserDao.fetchOneByEmail(email).getPassword();
            applicationUser=con.connection().fetchOne(APPLICATION_USER,APPLICATION_USER.EMAIL.eq(email)
                    .and(APPLICATION_USER.PASSWORD.eq(password)))
                    .into(ApplicationUser.class);
        } catch (Exception e) {
            applicationUser=null;
        }
        return applicationUser;
    }


    public Pagination<ApplicationUser> page(int page, int pageSize, String filter) {
        if (page < 1) {
            page = 1;
        }
        String vFilter = null==filter?"":filter.toLowerCase();
        List<ApplicationUser> applicationUsers = applicationUserDao.findAll();
        applicationUsers = applicationUsers.stream().filter(e -> (e.getEmail().toLowerCase().contains(vFilter))
                ||(e.getNom().toLowerCase().contains(vFilter))).collect(Collectors.toList());

        int total = applicationUsers.size();
        int minIndex=Math.min((page-1)*pageSize, total);
        int maxIndex=Math.min(page*pageSize, total);
        applicationUsers = applicationUsers.subList(minIndex,maxIndex);
        return new Pagination<>(applicationUsers, total, page, pageSize);
    }


    public boolean camparePassword(String confirmePassword, String password){
        if(null != confirmePassword && null!= password && confirmePassword.equals(password))
            return true;
        else
            return false;

    }


    public String changePassword(String email, String newPassword) {

        ApplicationUser user = applicationUserDao.fetchOneByEmail(email);
        if(null != user){
            user.setPassword(BCryptHash.hashPassword(newPassword));
            applicationUserDao.update(user);
            return "OK";
        }
        else
            return "Utilisateur introuvable";
    }

    /**
     *
     * @param query
     * @return
     */
    public List<ApplicationUser> findLikeUser(String query) {
        String luser = null == query ? "" : query;
        luser = luser.endsWith("%") ? luser : luser + "%";
        return this.con.connection().selectFrom(APPLICATION_USER)
                .where(APPLICATION_USER.EMAIL.like(luser)).fetchInto(ApplicationUser.class);
    }


    /**
     *
     * @return
     */
    public List<String> getUserProfil(){
        List<String> profilList =  this.con.connection().selectDistinct(APPLICATION_PROFIL.CODE)
                .from(APPLICATION_PROFIL)
                .orderBy(APPLICATION_PROFIL.CODE.asc())
                .fetchInto(String.class);

        return  profilList;
    }






    /**
     * Meke code routes
     * @param codeRoutes
     * @return
     */
    public String getCodeRoutes(String codeRoutes){
        String[] splitCodeRoutes = codeRoutes.trim().split("/");
        String fristLetter="",fristPart="";
        StringBuilder code = new StringBuilder();
        if(splitCodeRoutes.length >= 2) {
            for (int i = 2; i < splitCodeRoutes.length; i++) {
                if(!splitCodeRoutes[i].isEmpty() && splitCodeRoutes[i].length() >=2) {

                    StringBuilder s = new StringBuilder(splitCodeRoutes[i]);
                    fristLetter = String.valueOf(splitCodeRoutes[i].charAt(0));
                    s.setCharAt(0, fristLetter.toUpperCase().charAt(0));
                    code.append(s);


                }
            }
            fristPart = splitCodeRoutes[1].replace("/","");
        }


        if((null != code && !code.toString().isEmpty()) || splitCodeRoutes.length !=0) {
            String codeRoutesMaked = fristPart + code.toString().replace("Subaction","").replace("teId","te");
            String[] splitCodeRoute = codeRoutesMaked.trim().split("-");
            StringBuilder codeFinal = new StringBuilder();
            if (splitCodeRoute.length >= 2) {

                for (int j = 1; j < splitCodeRoute.length; j++) {
                    StringBuilder s = new StringBuilder(splitCodeRoute[j]);
                    fristLetter = String.valueOf(splitCodeRoute[j].charAt(0));
                    s.setCharAt(0, fristLetter.toUpperCase().charAt(0));
                    codeFinal.append(s);
                }

                fristPart = splitCodeRoute[0].replace("/","");
                return fristPart + codeFinal.toString();
            }else
                return codeRoutesMaked;

        }
        else
            return fristPart;
    }



}
