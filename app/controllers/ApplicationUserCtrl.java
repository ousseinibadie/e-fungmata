package controllers;


import commons.BCryptHash;
import controllers.util.Helpers;
import controllers.util.Secured;
import controllers.util.ViewMode;
import models.tables.pojos.ApplicationUser;
import models.tables.pojos.ApplicationUserProfil;
import org.joda.time.DateTime;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.ApplicationMainServices;


import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author abdoul-razack
 *
 */
//@Security.Authenticated(Secured.class)
public class ApplicationUserCtrl extends Controller {
	
	private final FormFactory formFactory;
	private final ApplicationMainServices applicationMainService;
	private Helpers helpers;
	
	private ApplicationUser appUser;
	private String passwordHash="";
	
	@Inject
	public ApplicationUserCtrl(Helpers helpers,
                               FormFactory formFactory,
                               ApplicationMainServices applicationMainService
							) {
		this.formFactory = formFactory;
		this.applicationMainService = applicationMainService;
		
	}
	
	/**
	 * 
	 * @return la liste des users
	 */
	public Result list(int page, String filter) {

			return ok(views.html.applicationUserList.render(
					applicationMainService.page(page, 10, filter),
					filter,
					Helpers.getCurrentUserType()));
		/*else
			return redirect(routes.LoginController.login());*/

	}
	
	/**
	 * 
	 * @param subAction
	 * @param userPK
	 * @return
	 */
	public Result show(String subAction, Long userPK) {
		ApplicationUser applicationUser;
		String viewMode;
		String[] userProfils={""};
		List<String> profilList = Arrays.asList(userProfils);

		if (null == userPK || userPK.equals(0L)) {
			applicationUser =(null == appUser) ? new ApplicationUser() : appUser;
			viewMode = ViewMode.VIEW_MODE_CREATE;
			passwordHash="";
		} else if (ViewMode.VIEW_MODE_EDIT.equals(subAction)) {
			applicationUser = applicationMainService.applicationUserDao.findById(userPK);
			passwordHash=applicationUser.getPassword();
			viewMode = ViewMode.VIEW_MODE_EDIT;
			userProfils = applicationUser.getMetaData().trim().split(",");
		} else if (ViewMode.VIEW_MODE_DELETE.equals(subAction)) {
			applicationUser = applicationMainService.applicationUserDao.findById(userPK);
			viewMode = ViewMode.VIEW_MODE_DELETE;
		} else {
			viewMode = ViewMode.VIEW_MODE_VIEW;
			applicationUser = applicationMainService.applicationUserDao.findById(userPK);
			userProfils = applicationUser.getMetaData().trim().split(",");
	           
		}

		profilList = (0==userProfils.length)? Arrays.asList(applicationUser.getMetaData()) : Arrays.asList(userProfils);

		return ok(views.html.applicationUserShow.render(applicationUser,
					viewMode,
					Helpers.getCurrentUserType(),
					applicationMainService.getUserProfil(),
					profilList));
		
		
	}
	
	/**
	 * 
	 * @return
	 */
	public Result save() {
        
        final String viewMode = formFactory.form().bindFromRequest().get("viewMode");
        final String[] userProfil = request().body().asFormUrlEncoded().get("profilUser");

        Form<ApplicationUser> uForm = formFactory.form(ApplicationUser.class).bindFromRequest();
        if (uForm.hasErrors()) {
       	 	flash("error","["+DateTime.now().toLocalTime()+"] "+ "Saisie erronée.");  
       	 	appUser = userSaisie(uForm.get());
       	 	return show(ViewMode.VIEW_MODE_CREATE,0L);
        }
        
        ApplicationUser applicationUser = uForm.get();
        
        String[] isActive = request().body().asFormUrlEncoded().get("isActive");
        String[] isAdmin = request().body().asFormUrlEncoded().get("isAdmin");
        // Utilisateur est actif
        if (isActive != null) {
        	applicationUser.setIsActive(true);
        }else
        	applicationUser.setIsActive(false);
        
        // Utilisateur est un admin
        if (isAdmin != null) {
        	applicationUser.setIsAdmin(true);
        }else
        	applicationUser.setIsAdmin(false);

		StringBuilder metaData= new StringBuilder();
		ApplicationUserProfil applicationUserProfil;

		for(String up : userProfil){
			if(metaData.toString().isEmpty())
				metaData.append(up);
			else
				metaData = metaData.append(",").append(up);
		}
        
        String message="";
		applicationUser.setMetaData(metaData.toString());
        if (ViewMode.VIEW_MODE_EDIT.equals(viewMode)) {
        	if(!passwordHash.equals(applicationUser.getPassword()))
				applicationUser.setPassword(BCryptHash.hashPassword(applicationUser.getPassword()));
        		try {
					applicationMainService.applicationUserDao.update(applicationUser);
					message = "OK";
				}catch (Exception e){
        			e.printStackTrace();
        			message = e.getMessage();
				}

			if(null != message && message.equals("OK")) {
				passwordHash="";
				applicationMainService.applicationUserProfilDao.delete(applicationMainService.applicationUserProfilDao.fetchByEmail(applicationUser.getEmail()));
				for (String up : userProfil){

					applicationUserProfil = new ApplicationUserProfil();
					applicationUserProfil.setEmail(applicationUser.getEmail());
					applicationUserProfil.setProfil(up);
					applicationUserProfil.setWhoDone("admin");
					try{
						applicationMainService.applicationUserProfilDao.insert(applicationUserProfil);
					}catch (Exception e){
						String error= e.getCause().getMessage();
						flash("error","["+DateTime.now().toLocalTime()+"] "+ "Echec de mise à jour de(s) profil(s): "+ error);
						return show(ViewMode.VIEW_MODE_EDIT,applicationUser.getId());
					}
				}
				flash("success", "[" + DateTime.now().toLocalTime() + "] " + "Mise à jour effectuée");
			}else
				flash("error","["+DateTime.now().toLocalTime()+"] "+ "Echec de mise à jour"); 
        	 return redirect(routes.ApplicationUserCtrl.list(0, ""));
        	
        }

		// Création d'un nouveau utilisateur
		try {
        	applicationUser.setPassword(BCryptHash.hashPassword(applicationUser.getPassword()));
			applicationMainService.applicationUserDao.insert(applicationUser);
			message = "OK";
		}catch (Exception e){
			e.printStackTrace();
			message = e.getMessage();
		}

		if(null != message && message.equals("OK")){
			for (String up : userProfil){
				applicationUserProfil = new ApplicationUserProfil();
				applicationUserProfil.setEmail(applicationUser.getEmail());
				applicationUserProfil.setProfil(up);
				applicationUserProfil.setWhoDone("admin");
				try{
					applicationMainService.applicationUserProfilDao.insert(applicationUserProfil);
					appUser = new ApplicationUser();
				}catch (Exception e){
					System.out.println(e.getCause().getMessage());
					appUser = userSaisie(applicationUser);
					flash("error","["+DateTime.now().toLocalTime()+"] "+ "Echec création de(s) profil(s)");
					return show(ViewMode.VIEW_MODE_CREATE,0L);
				}
			}
			flash("success","["+DateTime.now().toLocalTime()+"] "+ "Nouveau utilisateur créé.");   
		}else{
			appUser = userSaisie(applicationUser);
			flash("error","["+DateTime.now().toLocalTime()+"] "+ "Echec de création de l'utilisateur"); 
		}
		return show(ViewMode.VIEW_MODE_CREATE,0L);
	}
	
	/**
	 * 
	 * @param idUser
	 * @return
	 */
	public Result delete(Long idUser){
		ApplicationUser user = applicationMainService.applicationUserDao.findById(idUser);
		applicationMainService.applicationUserProfilDao.delete(applicationMainService.applicationUserProfilDao.fetchByEmail(user.getEmail()));
		String message = applicationMainService.delete(idUser);
		if(null != message && message.equals("OK"))
			flash("success","["+DateTime.now().toLocalTime()+"] "+ "Utilisateur :"+user.getEmail()+" a été supprimé"); 
		else
			flash("error","["+DateTime.now().toLocalTime()+"] "+ "Echec de la suppression!"); 
    	 return redirect(routes.ApplicationUserCtrl.list(0, ""));
		
	}
	
	/**
	 * 
	 * @return la page de changement de mot de passe
	 */
	public Result viewChangePassword(){
		return ok(views.html.changePassword.render(Helpers.getCurrentUserType()));
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Result changePassword() {
    	DynamicForm requestData = formFactory.form().bindFromRequest();
    	if (requestData.hasErrors()) {
    		flash("error","["+DateTime.now().toLocalTime()+"] "+ " Erreur de saisie");
            return ok(views.html.changePassword.render(Helpers.getCurrentUserType()));
        } else {
	    	String password = requestData.get("password");
	    	String nouveauPassword = requestData.get("nouveauPassword");
	    	String confirmePassword = requestData.get("confirmePassword");
	    	System.out.println("login: "+session("email")+" Mot de passe: "+password);
	    	ApplicationUser utilisateur = applicationMainService.authentification(session("email"), password);
	    	
	    	boolean goodPassword = applicationMainService.camparePassword(confirmePassword, nouveauPassword);
	    	if(!goodPassword || null == utilisateur){
	    		flash("error","["+DateTime.now().toLocalTime()+"] "+ "Mot de passe Incorrecte");
	    		return ok(views.html.changePassword.render(Helpers.getCurrentUserType()));
	    	}
	    	String message = applicationMainService.changePassword(session("email"), nouveauPassword);
	    	if(null != message && message.equals("OK")){
		        session().clear();
		        flash("success", "Votre mot de passe a été changé, veuillez vous reconnecter pour continuer.");
		        return redirect(routes.LoginController.login());
	    	}else{
	    		flash("error","["+DateTime.now().toLocalTime()+"] "+ "Echec, votre mot de passe n'a pas été changé");
	    		return ok(views.html.changePassword.render(Helpers.getCurrentUserType()));
	    	}
	    		
        }
    }
	
	
	private ApplicationUser userSaisie(ApplicationUser user){
		appUser = new ApplicationUser();
		appUser.setNom(user.getNom());
		appUser.setPrenom(user.getPrenom());
		appUser.setEmail(user.getEmail());
		appUser.setVille(user.getVille());
		appUser.setPays(user.getPays());
		appUser.setTel(user.getTel());
		appUser.setPassword(user.getPassword());
		appUser.setMetaData(user.getMetaData());
		appUser.setIsActive(user.getIsActive());
		appUser.setIsAdmin(user.getIsAdmin());
		return appUser;
	}

}
