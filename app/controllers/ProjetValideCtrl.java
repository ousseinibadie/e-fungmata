package controllers;

import com.google.inject.Inject;
import controllers.util.Secured;
import play.mvc.Controller;
import play.mvc.Security;
import services.ProjetMainServices;

@Security.Authenticated(Secured.class)
public class ProjetValideCtrl extends Controller {

    private final ProjetMainServices projetMainServices;

    @Inject
    public ProjetValideCtrl(ProjetMainServices projetMainServices){
        this.projetMainServices = projetMainServices;

    }
}
