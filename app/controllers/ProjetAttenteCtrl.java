package controllers;

import com.google.inject.Inject;
import controllers.util.Secured;
import controllers.util.ViewMode;
import models.tables.pojos.Investissement;
import models.tables.pojos.ProjetAtttente;
import org.joda.time.DateTime;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.ProjetMainServices;
import services.SecteurActiviteServices;
import services.TypeProjetServices;

import java.io.File;


@Security.Authenticated(Secured.class)
public class ProjetAttenteCtrl extends Controller {

    private final ProjetMainServices projetMainServices;
    private final TypeProjetServices typeProjetServices;
    private final SecteurActiviteServices typeFinancementServices;
    private final FormFactory formFactory;

    private static final String projectDocument = "document/";

    @Inject
    public ProjetAttenteCtrl(ProjetMainServices projetMainServices,
                                FormFactory formFactory,
                                TypeProjetServices typeProjetServices,
                                SecteurActiviteServices typeFinancementServices){

        this.projetMainServices = projetMainServices;
        this.formFactory = formFactory;
        this.typeProjetServices = typeProjetServices;
        this.typeFinancementServices = typeFinancementServices;
    }


    private Result redirectToDefaultList() {
        return redirect(routes.ProjetAttenteCtrl.list(0, ""));
    }


    /**
     *
     * @param page
     * @param filter
     * @return Liste des projets en attente de financement
     */
    public Result list(int page, String filter){
        return ok(views.html.projetAttenteList.render(projetMainServices.pageProjetAttente(page,10,filter)));
    }


    /**
     *
     * @param subAction
     * @param id
     * @return Formulaire de création ou modification d'un nouveau projet de financement
     */
    public Result show(String subAction, Long id){
        ProjetAtttente projetAtttente;
        String viewMode;

        switch (subAction){

            case ViewMode.VIEW_MODE_CREATE:
                projetAtttente = new ProjetAtttente();
                viewMode =  ViewMode.VIEW_MODE_CREATE;
                break;

            case ViewMode.VIEW_MODE_EDIT:
                projetAtttente = projetMainServices.projetAtttenteDao.findById(id);
                viewMode =  ViewMode.VIEW_MODE_EDIT;
                break;
            case ViewMode.VIEW_MODE_VIEW:
                projetAtttente = projetMainServices.projetAtttenteDao.findById(id);
                viewMode =  ViewMode.VIEW_MODE_VIEW;
                break;
            default:
                return redirectToDefaultList();
        }
        return ok(views.html.projetAttenteShow.render(projetAtttente,
                viewMode,
                typeProjetServices.options(),
                typeFinancementServices.options()));

    }


    /**
     *
     * @return Formulaire de creation
     */
   public Result saveOrUpdate(){
       //File document  = request().body().asRaw().asFile()

       final String viewMode = formFactory.form().bindFromRequest().get("viewMode");
       Form<ProjetAtttente> uForm = formFactory.form(ProjetAtttente.class).bindFromRequest();

       if (uForm.hasErrors()) {
           flash("error","["+ DateTime.now().toLocalTime()+"] "+ "Saisie erronée.");
           return show(ViewMode.VIEW_MODE_CREATE,0L);
       }

       ProjetAtttente projetAtttente = uForm.get();

       /*if (document != null) {
           String fileName = projectDocument+projetAtttente.getDocumentFournit();
           String contentType = document.getContentType();
           File file = document.getFile();
           file.mkdirs();
           projetAtttente.setDocumentFournit(fileName);
       } else {
           flash("error", "Missing file");
           return badRequest();
       }*/


       projetAtttente.setWhoDone("admin");
        // Mis à jour d'un projet en attente de financement
       if (ViewMode.VIEW_MODE_EDIT.equals(viewMode)) {
           try {
               projetMainServices.projetAtttenteDao.update(projetAtttente);
               flash("success","["+ DateTime.now().toLocalTime()+"] "+ "Mise à jour effectuée.");
           }catch (Exception e){
               e.printStackTrace();
               flash("error","["+ DateTime.now().toLocalTime()+"] "+ "Erreur du mise à du projet."+ e.getMessage());
           }
       }


       //Création d'un nouveau projet de financement
       try {
           projetMainServices.projetAtttenteDao.insert(projetAtttente);
           flash("success","["+ DateTime.now().toLocalTime()+"] "+ "Nouvelle soumission de projet effectuée.");
       }catch (Exception e){

       }


       return show(ViewMode.VIEW_MODE_CREATE,0L);
   }




   public Result projetInvestissement(){
       return ok(views.html.projetInvestissement.render(projetMainServices.vProjetAttenteFindAll()));
   }


    /**
     *
     * @param id
     * @return
     */
   public Result investir(){

       Form<Investissement> uForm = formFactory.form(Investissement.class).bindFromRequest();

       if (uForm.hasErrors()) {
           flash("error","["+ DateTime.now().toLocalTime()+"] "+ "Saisie erronée.");
           return redirect(routes.ProjetAttenteCtrl.projetInvestissement());
       }

       Investissement investissement = uForm.get();

       investissement.setWhoDone("admin");
        try {
            projetMainServices.investissementDao.insert(investissement);
            flash("success","["+ DateTime.now().toLocalTime()+"] "+ "Nouveau de projet.");
        }catch (Exception e){
            e.printStackTrace();
            flash("success","["+ DateTime.now().toLocalTime()+"] "+ "Nouvelle soumission de projet.");
        }



       return redirect(routes.ProjetAttenteCtrl.projetInvestissement());

   }



}
