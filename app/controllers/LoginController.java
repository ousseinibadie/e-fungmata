package controllers;


import com.google.inject.Inject;
import commons.Login;
import models.tables.pojos.ApplicationUser;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import services.ApplicationMainServices;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

public class LoginController extends Controller {


    private final FormFactory formFactory;
    private final ApplicationMainServices applicationMainService;


    //TODO @Khalid implemente session expiration, see local/kando cegib_budget

    @Inject
    public LoginController(FormFactory formFactory,
                           ApplicationMainServices applicationMainService) {

        this.applicationMainService = applicationMainService;
        this.formFactory = formFactory;

    }


    /**
     * @return l'utillisateur connecté
     */
    private ApplicationUser getUtilisateur() {
        return applicationMainService.applicationUserDao.fetchOneByEmail(session("email"));
    }


    /**
     * @return
     */
    public Result index() {
        if (session("email") == null) {
            return redirect(routes.LoginController.login());
        } else {

        }
            flash("error", "Utilisateur déconnecté, compte inactif ou erreur de connexion");
            return redirect(routes.LoginController.login());

    }


    /**
     * Formulaire d'authentification
     *
     * @return
     */
    public Result login() {
        if (null != session("email"))
            session().clear();
        return ok(views.html.login.render());
    }

    /**
     * Déconnexion d'un utilisateur
     *
     * @return
     */
    public Result logout() {
        session().clear();
        flash("success", "Vous êtes déconnectés");
        return redirect(routes.LoginController.login());
    }


    /**
     * @return
     */
    public Result authentification() {
        Form<Login> form = formFactory.form(Login.class).bindFromRequest();
        if (form.hasErrors()) {
            flash("error", " Erreur de saisie");
            return redirect(controllers.routes.LoginController.login());
        } else {
            Login login = form.get();
            ApplicationUser utilisateur = applicationMainService.authentification(login.getEmail(), login.getPassword());
            if (utilisateur == null ) {
                flash("error", "wrong email/password");
                return redirect(routes.LoginController.login());
            } else if(!utilisateur.getIsActive()){
                flash("error", "Votre compte est désactivé -  Contacter l'informatique SVP.");
                return redirect(routes.LoginController.login());
            }
            else {
                session("email", (utilisateur.getEmail()));
                return redirect(routes.HomeController.index());
            }
        }
    }

}
