package controllers;

import controllers.util.Secured;
import play.mvc.Controller;
import play.mvc.Security;

@Security.Authenticated(Secured.class)
public class GetSession extends Controller{

    public String getSession(){
        return session("email");
    }
}
