package controllers.util;


import com.google.inject.Inject;
import com.google.inject.Singleton;

import commons.UserProfileHelper;
import controllers.GetSession;
import models.tables.pojos.ApplicationUser;
import play.Logger;
import services.ApplicationMainServices;


import javax.annotation.Nonnull;
import java.util.*;

/**
 * @author ak
 * intanciated as eagerSingleton in modules.AppModule
 */

@Singleton
public class Helpers {


    /* les services précédent sont static, il est de loin préférable de les injecter directement sur les objets
    plutôt qu'à travers une instanciation du constructeur de cette classe*/
    private UserProfileHelper userProfileHelper;
    private ApplicationMainServices applicationMainServices;

    private static Helpers instance;

    @Inject
    public Helpers(UserProfileHelper helper,
                   ApplicationMainServices applicationMainServices) {

        this.userProfileHelper = helper;
        this. applicationMainServices = applicationMainServices;
        this.instance = this;
    }


    /**
     *
     * @param userEmail
     * @return
     */
    private static boolean isUserAdmin(String userEmail) {
        return instance.userProfileHelper.queryIsAdmin(userEmail);

    }


    private static boolean isUserPorteurProjet(String userEmail) {
        return instance.userProfileHelper.queryIsUserPorteurProjet(userEmail);

    }

    /**
     *
     * @param userEmail
     * @return
     */
    private static boolean isUserInvestisseur(String userEmail) {
        return instance.userProfileHelper.queryIsUserInvestisseur(userEmail);

    }

    /**
     *
     * @param userEmail
     * @return
     */
    private static boolean isUserEtablissement(String userEmail) {
        return instance.userProfileHelper.queryIsUserEtablissement(userEmail);

    }

    /**
     *
     * @param userEmail
     * @return
     */
    private static boolean isUserSuiviProjet(String userEmail) {
        return instance.userProfileHelper.queryIsUserSuiviProjet(userEmail);

    }





    /**
     * simple helper pour déterminer si l'utilisateur en cours est un porteur de projet
     * <p>
     * également appelé par les views
     *
     * @return true si l'utilisateur connecté est porteur projet
     */

    public static boolean isCurrentUserPosteurProjet() {
        String email = GetSession.session("email");
        if (null != email)
            return isUserPorteurProjet(email);
        return false;

    }


    /**
     * @return renvoi true si l'utilisateur est Investisseur
     */

    public static boolean isCurrentUserInvestissement() {
        String email = GetSession.session("email");
        if (null != email) {
            return isUserInvestisseur(email);
        }
        return false;

    }


    /**
     * @return renvoi true si l'utilisateur est Etablissement
     */

    public static boolean isCurrentUserEtablissement() {
        String email = GetSession.session("email");
        if (null != email) {
            return isUserEtablissement(email);
        }
        return false;

    }
    
    /**
     * 
     * @return
     */
    public static boolean isCurrentUserSuiviProjet(){
        String email = GetSession.session("email");
        if (null != email) {
            return isUserSuiviProjet(email);
        }
        return false;
    }


    public static boolean isCurrentUserAdmin(){
        String email = GetSession.session("email");
        if (null != email) {
            return isUserAdmin(email);
        }
        return false;
    }

   




    /**
     * returns a string with full user type (profile) name
     *
     * @return
     */

    public static String getCurrentUserType() {

        String response = "";

        if (isCurrentUserPosteurProjet()) {

            response = "Porteur projet";

        } else if (isCurrentUserInvestissement()) {

            response = "Investisseur";

        } else if (isCurrentUserEtablissement()) {

            response = "Etablissement";

        }else if (isCurrentUserSuiviProjet()) {

            response = "Suivi projet";

        }
        else {

            response = "Unknown";

        }

        return response;

    }


    /**
     *
     * @return user email
     */
    public static String getCurrentUserEmail(){
        return  GetSession.session("email");
    }

    /**
     *
     * @return current user connected
     */
    public static ApplicationUser getCurrentUser(){

        String email = GetSession.session("email");
        if (null != email) {
            return instance.applicationMainServices.applicationUserDao.fetchOneByEmail(email) ;
        }
        return new ApplicationUser();
    }




}
