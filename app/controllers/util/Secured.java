package controllers.util;

import com.typesafe.config.Config;
import controllers.routes;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Secured extends Security.Authenticator {
    public static final String SESSION_TIMEOUT_CONFIG_KEY = "application.session.timeout";
    public static final String USER_LAST_TIME = "userLastTime";
    public static final String USER_ID_FIELD = "email";

    private final long timeout;

    @Inject
    public Secured(Config config) {
        super();
        if (config.hasPath(SESSION_TIMEOUT_CONFIG_KEY)) {
            timeout = config.getDuration(SESSION_TIMEOUT_CONFIG_KEY, TimeUnit.MILLISECONDS);
        } else {
            timeout = 0L;
        }

    }

    @Override
    public String getUsername(Context ctx) {

        // check session expired
        if (timeout > 0L) {
            String previousTick = ctx.session().get(USER_LAST_TIME);
            if (previousTick != null && !previousTick.equals("")) {
                long previousT = Long.valueOf(previousTick);
                long currentT = new Date().getTime();
                if ((currentT - previousT) > timeout) {
                    // session expired
                    ctx.session().clear();
                    ctx.flash().put("error", "La session a expiré après une inactivité de plus de :" + timeout / 1000 / 60 + " minutes.");

                    return null;
                }
            }
            // update time in session
            String tickString = Long.toString(new Date().getTime());
            ctx.session().put(USER_LAST_TIME, tickString);

        }
        return ctx.session().get(USER_ID_FIELD); //-this is naturaly :=) set by login
    }

    @Override
    public Result onUnauthorized(Context ctx) {
        ctx.flash().put("error", "La session a expiré, veuillez vous connecter");
        return redirect(routes.LoginController.login());
    }
}
