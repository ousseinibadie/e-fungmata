package controllers.util;
/**
 * 
 * @author hassan
 *
 */

public class ViewMode {

	public static final String  VIEW_MODE_CREATE = "CREATE";
	public static final String VIEW_MODE_EDIT = "EDIT";
	public static final String VIEW_MODE_VIEW = "VIEW";
	public static final String VIEW_MODE_DELETE = "DELETE";
	public static final int pageSize = 5;
}
