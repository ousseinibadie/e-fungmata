package commons;

import com.google.inject.Inject;
import modules.IConnectionHelper;
import org.jooq.Configuration;

import java.util.HashMap;
import java.util.Map;

public class Utils {
	
	private IConnectionHelper con;
    private Configuration conf;
	
	@Inject
	public Utils(IConnectionHelper con) {
		this.con = con;
        this.conf = this.con.connection().configuration();
	}

    static public boolean isNullOrEmpty(String obj) {
        if (null == obj || obj.trim().isEmpty()) return true;
        return false;
    }

}
