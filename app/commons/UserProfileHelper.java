package commons;


import com.google.inject.Inject;

import models.tables.daos.ApplicationUserDao;
import models.tables.daos.ApplicationUserProfilDao;
import models.tables.pojos.ApplicationUser;
import models.tables.pojos.ApplicationUserProfil;
import modules.IConnectionHelper;

import org.jooq.Configuration;

import java.util.List;


public class UserProfileHelper {

    private IConnectionHelper con;

    private Configuration conf;

    private final ApplicationUserDao applicationUserDao;
    private final ApplicationUserProfilDao applicationUserProfilDao;


    private final String UserProcessEmail = "=>process@finances.gov.ne";

    /**
     * utilisation essentiellement en mode static du côté client
     * on n'a donc pas à l'injecter mais à la déclarer static
     */
    @Inject
    public UserProfileHelper(IConnectionHelper con) {
        this.con = con;
        this.conf = this.con.connection().configuration();
        this.applicationUserDao = new ApplicationUserDao(conf);
        this.applicationUserProfilDao = new ApplicationUserProfilDao(conf);
    }

    /**
     * checks if a user identiefid by userMail has the given profile
     * users' profiles are set in application_user
     *
     * @param userEmail user's login
     * @param profile   user's profile
     * @author abdoul-razack
     * @since 1.1 -- 20181003
     */
    private boolean isUserOfProfile(String userEmail, String profile) {
        boolean result = false;
        ApplicationUser applicationUser = applicationUserDao.fetchOneByEmail(userEmail);
        List<ApplicationUserProfil> userProfilList = applicationUserProfilDao.fetchByEmail(userEmail);

        if (0 != userProfilList.size() && null!= applicationUser) {
            for (ApplicationUserProfil userProfil : userProfilList) {
                if (applicationUser.getIsActive() && null != userProfil.getProfil() && userProfil.getProfil().equals(profile)) {
                    result = true;
                    return result;
                }
            }
        }
        return result;
    }

    /**
     * @param userEmail
     * @return
     */
    public boolean queryIsUserPorteurProjet(String userEmail) {
        return isUserOfProfile(userEmail, "porteur_projet");
    }


    /**
     * @param userEmail
     * @return
     */
    public boolean queryIsUserInvestisseur(String userEmail) {
        return isUserOfProfile(userEmail, "investisseur");
    }

    /**
     *
     * @param userEmail
     * @return
     */

    public boolean queryIsUserEtablissement(String userEmail) {
        return isUserOfProfile(userEmail, "etablissement");
    }

    /**
     *
     * @param userEmail
     * @return
     */
    public boolean queryIsUserSuiviProjet(String userEmail) {
        return isUserOfProfile(userEmail, "suivi_projet");
    }


    /**
     * @param userEmail
     * @return
     */
    public boolean queryIsAdmin(String userEmail) {
        boolean result = false;
        ApplicationUser applicationUser = applicationUserDao.fetchOneByEmail(userEmail);
        if (null != applicationUser) {
            if (null != applicationUser.getIsActive() && applicationUser.getIsActive()
                    && null != applicationUser.getIsAdmin() && applicationUser.getIsAdmin()) {
                result = true;
            }
        }
        return result;
    }


}