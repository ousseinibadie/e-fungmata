package modules;

import org.jooq.DSLContext;

/**
 * ce type d'interface est définie dans le module expenditure-model sous le nom {@link IConnectionHelper}
 * et est fourni à travers le jar dans lib
 *
 * on pourrait en faire un jar à part (api) et l'installer à la fois dans model et app
 */
public interface SampleConnectionHelper {
    DSLContext connection();
}
