package modules;

//import api.ILoginService;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import org.jooq.DSLContext;

//import play.data.format.Formatters;
//import util.IViewAttribut;
//import util.LoginService;
//import util.ViewAttribut;

public class AppModule extends AbstractModule {
    @Override
    public void configure() {
        // related to forms.get field with javatime
        // for usage see local/kando git cegib_budget bind(Formatters.class).toProvider(FormattersProvider.class);
        // related to automaticaly setting id field to ro in views
        //bind(IViewAttribut.class).to(ViewAttribut.class).in(Scopes.SINGLETON);

        //TODO implement session expiration @khalid see local/kando cegib_budget (models)
        //bind(ILoginService.class).to(LoginService.class).in(Scopes.SINGLETON);

        //bind(OnStartUp.class).asEagerSingleton();
        bind(DSLContext.class).toProvider(DSLContextProvider.class).in(Scopes.SINGLETON);
        bind(IConnectionHelper.class).toProvider(ConnectionHelperAppImp.class);
        bind(modules.ConnectionProvider.class);
        //bind(controllers.util.CtrlHelpers.class).asEagerSingleton();
        bind(controllers.util.Helpers.class).asEagerSingleton();

        //NOT NEED FOR THIS PROJECT bind(IDSLContextCreator.class).to(DSLContextCreator.class);

//        install(new FactoryModuleBuilder()r
//                .implement(IDSLContextCreator.class, DSLContextCreator.class)
//                .build(IDSLContextCreatorFactory_DELETE_THIS.class));
    }

}
