# e-fundMata

# What is this?
    • It is a  web application !
    • Version 1.0.

# What is E-FUNDMATA ?
    • E-FUNDMATA is a web application that allows women to expose their projects for funding by a donor, so donors have the opportunity to fund the project of their choice.

    • It is a simple and powerful web server application which requires a server with FRAMEWORK PLAY, langages JAVA SCALA for backend and JAVASCRIPT for frontend  and either database management system (DBMS)  PostgreSQL to run.
# Is E-FUNDMATA for you?
E-FUNDMATA is the solution to increase productivity, women's productive capacity to improve their income level and contribute effectively, in all spheres of social, economic, political and cultural life.
# What are the benefits of E-FUNDMATA ?
    • With E-FUNDMATA, invest simply in the young companies of the future of Nigerian women. You are free to choose the startups you want to support.
    • In just a few clicks, finance companies with high growth potential and become an integral part of the entrepreneurial adventure.
    • It aims in particular to democratize access to financing the economy for women.
E-FUNDMATA is the ideal alternative to traditional financing channels.
# Ready to install E-FUNDMATA ?
[Play Framework 2.6.x](https://www.playframework.com/documentation/2.6.x/)
# Copyright
Copyright (C) 2019 Hackathongenre.

