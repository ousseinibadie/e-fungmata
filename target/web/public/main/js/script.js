/*
 * @auteur: CHAMA Zakari
 */


/********* fonction affichage programmes en fonction de la section ************/
function getProgrammesBySection(section){
	$('#programme').empty();
	 $.getJSON('/Administration/programme.json/'+section, function(data){
		$.each(data, function(name, value) {   
			 $('#programme').append($('<option>', { 
			        value: value.code,
			        text : value.code+"--"+value.intitule
			    }));

		});

	 });
}
/********* fonction affichage programmes en fonction de la section ************/


/********* fonctions pour la pagination des blocs latéraux ************/
/*if ($('.container').hasClass('indexAdmin')){
	show_per_page = 13;
}*/
var show_per_page = 5;
var current_page = 0;

function set_display(first, last) {
	$('#content').children().css('display', 'none');
	$('#content').children().slice(first, last).css('display','block');
	
}

function previous(){
	if($('.active').prev('.page_link').length)
	go_to_page(current_page - 1);
}

function next(){
	if($('.active').next('.page_link').length)
	go_to_page(current_page + 1);
}

function go_to_page(page_num){
	current_page = page_num;
	start_from = current_page * show_per_page;
	end_on = start_from + show_per_page;
	set_display(start_from, end_on);
	$('.active').removeClass('active');
	$('#id' + page_num).addClass('active');
	$('.'+$('.programmeCode').val()).addClass('active');
	$('.'+$('.actionCode').val()).addClass('active');
	$('.'+$('.activiteCode').val()).addClass('active');
	//alert(show_per_page);
}
/********* fonctions pour la pagination des blocs latéraux ************/

$(function() {
	
	/********* Accordéon pour la synthèse des programmes directeur financier ************/
	var icons = {
	          header: "ui-icon-circle-arrow-e",
	          activeHeader: "ui-icon-circle-arrow-s"
	        };
	$('.synthese-prog').accordion({
		icons: icons,
		collapsible: true,
	    heightStyle: 'content'
	});
	/********* Accordéon pour la synthèse des programmes directeur financier ************/
	
	
	/********* Affichage de la section et/ou du programme en fonction de l'utilisateur ************/
	switch ($('.profil_val').val()) {
	case 'directeur_financier':
		$(".prg_section").hide();
		$(".prg_section1").show();
		break;
	case 'charge_programme':
		$(".prg_section").show();
		break;
	default:
		$(".prg_section").hide();
		break;
	}
	
	$('#profilCode').change(function(){
		var pr=$("#profilCode").val();
		//alert(pr);
		if(pr=="charge_programme" || pr=="saisie_programme"){
			$(".prg_section").show();
		}else if( pr=="directeur_financier"){
			$(".prg_section").hide();
			$(".prg_section1").show();
	    }else{
			$(".prg_section").hide();
		}
	});
	
	getProgrammesBySection($('#section').val());
	
	$('#section').change(function(){
		var section =$(this).val();
		getProgrammesBySection(section);
	});
	/********* Affichage de la section et/ou du programme en fonction de l'utilisateur ************/
	
	
	/********* Envoi du formulaire pour la modification du mot de passe ************/
	$('.save-password').click(function(){
		$('#mod-pass').trigger('submit');
	});
	/********* Envoi du formulaire pour la modification du mot de passe ************/
	
	
	/********* Pagination des blocs latéraux ************/
	var number_of_pages = Math.ceil($('#childLength').val() / show_per_page);
	var goto = Math.floor($('.indexCode').val() / show_per_page);
	var nav = '<ul class="pagination"><li><a title="Precédent" href="javascript:previous();"><<</a></li>';
	var i = -1;
	
	while(number_of_pages > ++i){
		nav += '<li class="page_link'
		if(!i) nav += ' active';
		nav += '" id="id' + i +'">';
		nav += '<a title="Aller à '+(i + 1)+'" href="javascript:go_to_page(' + i +')">'+ (i + 1)
		+'</a></li>';
	}
	nav += '<li><a title="Suivant" href="javascript:next();">>></a></li></ul>';
	if ($('#childLength').val() > show_per_page){
		$('#page_navigation').html(nav);
	}
	go_to_page(goto);
	/********* Pagination des blocs latéraux ************/
});