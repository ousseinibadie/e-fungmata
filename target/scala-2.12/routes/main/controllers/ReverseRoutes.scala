
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/conf/routes
// @DATE:Sun Apr 28 12:47:54 WAT 2019

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:32
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:32
    def versioned(file:Asset): Call = {
      implicit val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public")))
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:15
  class ReverseApplicationUserCtrl(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:18
    def delete(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "utilisateur/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:19
    def viewChangePassword(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "utilisateur/view/changePassword")
    }
  
    // @LINE:16
    def show(subaction:String, userPK:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "utilisateur/show/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("subaction", subaction)) + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[Long]].unbind("userPK", userPK)))))
    }
  
    // @LINE:20
    def changePassword(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "utilisateur/changePassword")
    }
  
    // @LINE:17
    def save(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "utilisateur/save")
    }
  
    // @LINE:15
    def list(page:Int = 0, filter:String = ""): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "utilisateurs/list" + play.core.routing.queryString(List(if(page == 0) None else Some(implicitly[play.api.mvc.QueryStringBindable[Int]].unbind("page", page)), if(filter == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("filter", filter)))))
    }
  
  }

  // @LINE:23
  class ReverseProjetAttenteCtrl(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:25
    def saveOrUpdate(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "projet-attente/save/update")
    }
  
    // @LINE:27
    def investir(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "projet-attente/investir")
    }
  
    // @LINE:26
    def projetInvestissement(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "projet-attente/investissement")
    }
  
    // @LINE:24
    def show(subaction:String, userPK:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "projet-attente/show/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("subaction", subaction)) + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[Long]].unbind("userPK", userPK)))))
    }
  
    // @LINE:23
    def list(page:Int = 0, filter:String = ""): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "projet-attente/list" + play.core.routing.queryString(List(if(page == 0) None else Some(implicitly[play.api.mvc.QueryStringBindable[Int]].unbind("page", page)), if(filter == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("filter", filter)))))
    }
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:9
  class ReverseLoginController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def authentification(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "connexion")
    }
  
    // @LINE:12
    def logout(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "logout")
    }
  
    // @LINE:9
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
    // @LINE:10
    def login(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "login")
    }
  
  }


}
