
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/conf/routes
// @DATE:Sun Apr 28 12:47:54 WAT 2019

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:32
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:32
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:15
  class ReverseApplicationUserCtrl(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:18
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ApplicationUserCtrl.delete",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "utilisateur/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:19
    def viewChangePassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ApplicationUserCtrl.viewChangePassword",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "utilisateur/view/changePassword"})
        }
      """
    )
  
    // @LINE:16
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ApplicationUserCtrl.show",
      """
        function(subaction0,userPK1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "utilisateur/show/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("subaction", subaction0)) + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[Long]].javascriptUnbind + """)("userPK", userPK1)])})
        }
      """
    )
  
    // @LINE:20
    def changePassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ApplicationUserCtrl.changePassword",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "utilisateur/changePassword"})
        }
      """
    )
  
    // @LINE:17
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ApplicationUserCtrl.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "utilisateur/save"})
        }
      """
    )
  
    // @LINE:15
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ApplicationUserCtrl.list",
      """
        function(page0,filter1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "utilisateurs/list" + _qS([(page0 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[Int]].javascriptUnbind + """)("page", page0)), (filter1 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("filter", filter1))])})
        }
      """
    )
  
  }

  // @LINE:23
  class ReverseProjetAttenteCtrl(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:25
    def saveOrUpdate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProjetAttenteCtrl.saveOrUpdate",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "projet-attente/save/update"})
        }
      """
    )
  
    // @LINE:27
    def investir: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProjetAttenteCtrl.investir",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "projet-attente/investir"})
        }
      """
    )
  
    // @LINE:26
    def projetInvestissement: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProjetAttenteCtrl.projetInvestissement",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "projet-attente/investissement"})
        }
      """
    )
  
    // @LINE:24
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProjetAttenteCtrl.show",
      """
        function(subaction0,userPK1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "projet-attente/show/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("subaction", subaction0)) + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[Long]].javascriptUnbind + """)("userPK", userPK1)])})
        }
      """
    )
  
    // @LINE:23
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProjetAttenteCtrl.list",
      """
        function(page0,filter1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "projet-attente/list" + _qS([(page0 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[Int]].javascriptUnbind + """)("page", page0)), (filter1 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("filter", filter1))])})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:9
  class ReverseLoginController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def authentification: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoginController.authentification",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "connexion"})
        }
      """
    )
  
    // @LINE:12
    def logout: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoginController.logout",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
        }
      """
    )
  
    // @LINE:9
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoginController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
    // @LINE:10
    def login: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoginController.login",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
        }
      """
    )
  
  }


}
