
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/conf/routes
// @DATE:Sun Apr 28 12:47:54 WAT 2019

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseApplicationUserCtrl ApplicationUserCtrl = new controllers.ReverseApplicationUserCtrl(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseProjetAttenteCtrl ProjetAttenteCtrl = new controllers.ReverseProjetAttenteCtrl(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLoginController LoginController = new controllers.ReverseLoginController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseApplicationUserCtrl ApplicationUserCtrl = new controllers.javascript.ReverseApplicationUserCtrl(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseProjetAttenteCtrl ProjetAttenteCtrl = new controllers.javascript.ReverseProjetAttenteCtrl(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLoginController LoginController = new controllers.javascript.ReverseLoginController(RoutesPrefix.byNamePrefix());
  }

}
