
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/conf/routes
// @DATE:Sun Apr 28 12:47:54 WAT 2019

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_1: controllers.HomeController,
  // @LINE:9
  LoginController_4: controllers.LoginController,
  // @LINE:15
  ApplicationUserCtrl_0: controllers.ApplicationUserCtrl,
  // @LINE:23
  ProjetAttenteCtrl_2: controllers.ProjetAttenteCtrl,
  // @LINE:32
  Assets_3: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_1: controllers.HomeController,
    // @LINE:9
    LoginController_4: controllers.LoginController,
    // @LINE:15
    ApplicationUserCtrl_0: controllers.ApplicationUserCtrl,
    // @LINE:23
    ProjetAttenteCtrl_2: controllers.ProjetAttenteCtrl,
    // @LINE:32
    Assets_3: controllers.Assets
  ) = this(errorHandler, HomeController_1, LoginController_4, ApplicationUserCtrl_0, ProjetAttenteCtrl_2, Assets_3, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_1, LoginController_4, ApplicationUserCtrl_0, ProjetAttenteCtrl_2, Assets_3, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix, """controllers.LoginController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.LoginController.login()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """connexion""", """controllers.LoginController.authentification()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logout""", """controllers.LoginController.logout()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """utilisateurs/list""", """controllers.ApplicationUserCtrl.list(page:Int ?= 0, filter:String ?= "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """utilisateur/show/""" + "$" + """subaction<[^/]+>""", """controllers.ApplicationUserCtrl.show(subaction:String, userPK:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """utilisateur/save""", """controllers.ApplicationUserCtrl.save()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """utilisateur/delete/""" + "$" + """id<[^/]+>""", """controllers.ApplicationUserCtrl.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """utilisateur/view/changePassword""", """controllers.ApplicationUserCtrl.viewChangePassword()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """utilisateur/changePassword""", """controllers.ApplicationUserCtrl.changePassword()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """projet-attente/list""", """controllers.ProjetAttenteCtrl.list(page:Int ?= 0, filter:String ?= "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """projet-attente/show/""" + "$" + """subaction<[^/]+>""", """controllers.ProjetAttenteCtrl.show(subaction:String, userPK:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """projet-attente/save/update""", """controllers.ProjetAttenteCtrl.saveOrUpdate()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """projet-attente/investissement""", """controllers.ProjetAttenteCtrl.projetInvestissement()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """projet-attente/investir""", """controllers.ProjetAttenteCtrl.investir()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_1.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_LoginController_index1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_LoginController_index1_invoker = createInvoker(
    LoginController_4.index(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoginController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """Ressources connexion""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_LoginController_login2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_LoginController_login2_invoker = createInvoker(
    LoginController_4.login(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoginController",
      "login",
      Nil,
      "GET",
      this.prefix + """login""",
      """""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_LoginController_authentification3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("connexion")))
  )
  private[this] lazy val controllers_LoginController_authentification3_invoker = createInvoker(
    LoginController_4.authentification(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoginController",
      "authentification",
      Nil,
      "POST",
      this.prefix + """connexion""",
      """""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_LoginController_logout4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logout")))
  )
  private[this] lazy val controllers_LoginController_logout4_invoker = createInvoker(
    LoginController_4.logout(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoginController",
      "logout",
      Nil,
      "GET",
      this.prefix + """logout""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_ApplicationUserCtrl_list5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("utilisateurs/list")))
  )
  private[this] lazy val controllers_ApplicationUserCtrl_list5_invoker = createInvoker(
    ApplicationUserCtrl_0.list(fakeValue[Int], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ApplicationUserCtrl",
      "list",
      Seq(classOf[Int], classOf[String]),
      "GET",
      this.prefix + """utilisateurs/list""",
      """ Ressources Application User""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_ApplicationUserCtrl_show6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("utilisateur/show/"), DynamicPart("subaction", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ApplicationUserCtrl_show6_invoker = createInvoker(
    ApplicationUserCtrl_0.show(fakeValue[String], fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ApplicationUserCtrl",
      "show",
      Seq(classOf[String], classOf[Long]),
      "GET",
      this.prefix + """utilisateur/show/""" + "$" + """subaction<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_ApplicationUserCtrl_save7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("utilisateur/save")))
  )
  private[this] lazy val controllers_ApplicationUserCtrl_save7_invoker = createInvoker(
    ApplicationUserCtrl_0.save(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ApplicationUserCtrl",
      "save",
      Nil,
      "POST",
      this.prefix + """utilisateur/save""",
      """""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_ApplicationUserCtrl_delete8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("utilisateur/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ApplicationUserCtrl_delete8_invoker = createInvoker(
    ApplicationUserCtrl_0.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ApplicationUserCtrl",
      "delete",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """utilisateur/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_ApplicationUserCtrl_viewChangePassword9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("utilisateur/view/changePassword")))
  )
  private[this] lazy val controllers_ApplicationUserCtrl_viewChangePassword9_invoker = createInvoker(
    ApplicationUserCtrl_0.viewChangePassword(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ApplicationUserCtrl",
      "viewChangePassword",
      Nil,
      "GET",
      this.prefix + """utilisateur/view/changePassword""",
      """""",
      Seq()
    )
  )

  // @LINE:20
  private[this] lazy val controllers_ApplicationUserCtrl_changePassword10_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("utilisateur/changePassword")))
  )
  private[this] lazy val controllers_ApplicationUserCtrl_changePassword10_invoker = createInvoker(
    ApplicationUserCtrl_0.changePassword(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ApplicationUserCtrl",
      "changePassword",
      Nil,
      "POST",
      this.prefix + """utilisateur/changePassword""",
      """""",
      Seq()
    )
  )

  // @LINE:23
  private[this] lazy val controllers_ProjetAttenteCtrl_list11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("projet-attente/list")))
  )
  private[this] lazy val controllers_ProjetAttenteCtrl_list11_invoker = createInvoker(
    ProjetAttenteCtrl_2.list(fakeValue[Int], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProjetAttenteCtrl",
      "list",
      Seq(classOf[Int], classOf[String]),
      "GET",
      this.prefix + """projet-attente/list""",
      """ Ressources Projet""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_ProjetAttenteCtrl_show12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("projet-attente/show/"), DynamicPart("subaction", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProjetAttenteCtrl_show12_invoker = createInvoker(
    ProjetAttenteCtrl_2.show(fakeValue[String], fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProjetAttenteCtrl",
      "show",
      Seq(classOf[String], classOf[Long]),
      "GET",
      this.prefix + """projet-attente/show/""" + "$" + """subaction<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:25
  private[this] lazy val controllers_ProjetAttenteCtrl_saveOrUpdate13_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("projet-attente/save/update")))
  )
  private[this] lazy val controllers_ProjetAttenteCtrl_saveOrUpdate13_invoker = createInvoker(
    ProjetAttenteCtrl_2.saveOrUpdate(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProjetAttenteCtrl",
      "saveOrUpdate",
      Nil,
      "POST",
      this.prefix + """projet-attente/save/update""",
      """""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_ProjetAttenteCtrl_projetInvestissement14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("projet-attente/investissement")))
  )
  private[this] lazy val controllers_ProjetAttenteCtrl_projetInvestissement14_invoker = createInvoker(
    ProjetAttenteCtrl_2.projetInvestissement(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProjetAttenteCtrl",
      "projetInvestissement",
      Nil,
      "GET",
      this.prefix + """projet-attente/investissement""",
      """""",
      Seq()
    )
  )

  // @LINE:27
  private[this] lazy val controllers_ProjetAttenteCtrl_investir15_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("projet-attente/investir")))
  )
  private[this] lazy val controllers_ProjetAttenteCtrl_investir15_invoker = createInvoker(
    ProjetAttenteCtrl_2.investir(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProjetAttenteCtrl",
      "investir",
      Nil,
      "POST",
      this.prefix + """projet-attente/investir""",
      """""",
      Seq()
    )
  )

  // @LINE:32
  private[this] lazy val controllers_Assets_versioned16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned16_invoker = createInvoker(
    Assets_3.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_1.index)
      }
  
    // @LINE:9
    case controllers_LoginController_index1_route(params) =>
      call { 
        controllers_LoginController_index1_invoker.call(LoginController_4.index())
      }
  
    // @LINE:10
    case controllers_LoginController_login2_route(params) =>
      call { 
        controllers_LoginController_login2_invoker.call(LoginController_4.login())
      }
  
    // @LINE:11
    case controllers_LoginController_authentification3_route(params) =>
      call { 
        controllers_LoginController_authentification3_invoker.call(LoginController_4.authentification())
      }
  
    // @LINE:12
    case controllers_LoginController_logout4_route(params) =>
      call { 
        controllers_LoginController_logout4_invoker.call(LoginController_4.logout())
      }
  
    // @LINE:15
    case controllers_ApplicationUserCtrl_list5_route(params) =>
      call(params.fromQuery[Int]("page", Some(0)), params.fromQuery[String]("filter", Some(""))) { (page, filter) =>
        controllers_ApplicationUserCtrl_list5_invoker.call(ApplicationUserCtrl_0.list(page, filter))
      }
  
    // @LINE:16
    case controllers_ApplicationUserCtrl_show6_route(params) =>
      call(params.fromPath[String]("subaction", None), params.fromQuery[Long]("userPK", None)) { (subaction, userPK) =>
        controllers_ApplicationUserCtrl_show6_invoker.call(ApplicationUserCtrl_0.show(subaction, userPK))
      }
  
    // @LINE:17
    case controllers_ApplicationUserCtrl_save7_route(params) =>
      call { 
        controllers_ApplicationUserCtrl_save7_invoker.call(ApplicationUserCtrl_0.save())
      }
  
    // @LINE:18
    case controllers_ApplicationUserCtrl_delete8_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_ApplicationUserCtrl_delete8_invoker.call(ApplicationUserCtrl_0.delete(id))
      }
  
    // @LINE:19
    case controllers_ApplicationUserCtrl_viewChangePassword9_route(params) =>
      call { 
        controllers_ApplicationUserCtrl_viewChangePassword9_invoker.call(ApplicationUserCtrl_0.viewChangePassword())
      }
  
    // @LINE:20
    case controllers_ApplicationUserCtrl_changePassword10_route(params) =>
      call { 
        controllers_ApplicationUserCtrl_changePassword10_invoker.call(ApplicationUserCtrl_0.changePassword())
      }
  
    // @LINE:23
    case controllers_ProjetAttenteCtrl_list11_route(params) =>
      call(params.fromQuery[Int]("page", Some(0)), params.fromQuery[String]("filter", Some(""))) { (page, filter) =>
        controllers_ProjetAttenteCtrl_list11_invoker.call(ProjetAttenteCtrl_2.list(page, filter))
      }
  
    // @LINE:24
    case controllers_ProjetAttenteCtrl_show12_route(params) =>
      call(params.fromPath[String]("subaction", None), params.fromQuery[Long]("userPK", None)) { (subaction, userPK) =>
        controllers_ProjetAttenteCtrl_show12_invoker.call(ProjetAttenteCtrl_2.show(subaction, userPK))
      }
  
    // @LINE:25
    case controllers_ProjetAttenteCtrl_saveOrUpdate13_route(params) =>
      call { 
        controllers_ProjetAttenteCtrl_saveOrUpdate13_invoker.call(ProjetAttenteCtrl_2.saveOrUpdate())
      }
  
    // @LINE:26
    case controllers_ProjetAttenteCtrl_projetInvestissement14_route(params) =>
      call { 
        controllers_ProjetAttenteCtrl_projetInvestissement14_invoker.call(ProjetAttenteCtrl_2.projetInvestissement())
      }
  
    // @LINE:27
    case controllers_ProjetAttenteCtrl_investir15_route(params) =>
      call { 
        controllers_ProjetAttenteCtrl_investir15_invoker.call(ProjetAttenteCtrl_2.investir())
      }
  
    // @LINE:32
    case controllers_Assets_versioned16_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned16_invoker.call(Assets_3.versioned(path, file))
      }
  }
}
