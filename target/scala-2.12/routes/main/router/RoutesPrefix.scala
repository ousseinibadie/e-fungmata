
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/conf/routes
// @DATE:Sun Apr 28 12:47:54 WAT 2019


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
