
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object applicationUserShow extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template5[models.tables.pojos.ApplicationUser,String,String,List[String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(
	applicationUser: models.tables.pojos.ApplicationUser,
	viewMode: String,
	profil: String,
    profilList : List[String],
	userProfilList : List[String]
):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*23.2*/import views.html.utils._
/*24.2*/import helper._

def /*10.2*/quote/*10.7*/(s: String) = {{
    "" + s + ""
}};def /*13.2*/isDisabled/*13.12*/(fieldName: String) = {{
    fieldName match {
        case "applicationUser" => if(null == applicationUser.getId)
            "name=" + quote(fieldName) else "name=" + quote(fieldName) + " disabled=" + quote("disabled")
        case _ => if((viewMode != "VIEW") && (viewMode != "DELETE"))
            "name=" + quote(fieldName) else "name=" + quote(fieldName) + " disabled=" + quote("disabled")

    }
}};
Seq[Any](format.raw/*7.2*/("""


"""),format.raw/*12.2*/("""
"""),format.raw/*21.2*/("""

"""),_display_(/*25.2*/main(if(viewMode == "CREATE") "Nouveau utilisateur" else if(viewMode == "VIEW") "Consultation utilisateur" else if(viewMode == "EDIT") "Edition utilisateur" else if(viewMode == "DELETE") "Suppression utilisateur" else "")/*25.223*/ {_display_(Seq[Any](format.raw/*25.225*/("""

"""),format.raw/*27.1*/("""<div class="container">

	<div class="panel panel-default">
		<label class="label label-info" style="float: right;"></label>
		"""),_display_(/*31.4*/if(viewMode == "CREATE")/*31.28*/{_display_(Seq[Any](format.raw/*31.29*/("""
		"""),format.raw/*32.3*/("""<div class="badged-title">Inscription</div>

		""")))}/*34.4*/else if(viewMode == "EDIT")/*34.31*/{_display_(Seq[Any](format.raw/*34.32*/("""
		"""),format.raw/*35.3*/("""<div class="badged-title">Edition</div>

		""")))}/*37.4*/else if(viewMode == "VIEW")/*37.31*/{_display_(Seq[Any](format.raw/*37.32*/("""
		"""),format.raw/*38.3*/("""<div class="badged-title">Détails du compte</div>

		""")))}),format.raw/*40.4*/("""
	    """),_display_(/*41.7*/form(routes.ApplicationUserCtrl.save())/*41.46*/{_display_(Seq[Any](format.raw/*41.47*/("""
		"""),_display_(/*42.4*/helper/*42.10*/.CSRF.formField),format.raw/*42.25*/("""
		 """),format.raw/*43.4*/("""<input type="hidden" name="viewMode" value=""""),_display_(/*43.49*/viewMode),format.raw/*43.57*/("""">
		 <input type="hidden" name="id" value=""""),_display_(/*44.43*/applicationUser/*44.58*/.getId),format.raw/*44.64*/("""">
	    <div class="prog_cat">
	    	<br />
			<div class="row">
				<div id="nameUser" class="col-md-6">
					<span class="glyphicon glyphicon-asterisk asterix"></span><label class="control-label" for="">Nom :  </label>
					<input placeholder="votre nom" id="name" name="nom" """),_display_(/*50.59*/isDisabled("nom")),format.raw/*50.76*/(""" """),format.raw/*50.77*/("""class="form-control"  value=""""),_display_(/*50.107*/applicationUser/*50.122*/.getNom()),format.raw/*50.131*/("""" autocomplete="off" type="text" required="required">
				</div> 
			</div>
			<div class="row">
				<div id="" class="col-md-6">
					<span class="glyphicon glyphicon-asterisk asterix"></span><label class="control-label" for="">Prénom:  </label>
					<input id="prenom" placeholder="votre prénom" name="prenom" """),_display_(/*56.67*/isDisabled("prenom")),format.raw/*56.87*/(""" """),format.raw/*56.88*/("""class="form-control"  value=""""),_display_(/*56.118*/applicationUser/*56.133*/.getNom()),format.raw/*56.142*/("""" autocomplete="off" type="text" required="required">
				</div>
			</div>
			<div class="row">
				<div id="emailUser" class="col-md-6">
					<span class="glyphicon glyphicon-asterisk asterix"></span><label class="control-label" for="">Adresse email:  </label>
					<input id="email" placeholder="adresse email: exemple@gmail.com" name="email" """),_display_(/*62.86*/isDisabled("email")),format.raw/*62.105*/(""" """),_display_(/*62.107*/isDisabled("password")),format.raw/*62.129*/(""" """),_display_(/*62.131*/if(viewMode == "EDIT")/*62.153*/{_display_(Seq[Any](format.raw/*62.154*/("""readonly="readonly"""")))}),format.raw/*62.174*/(""" """),format.raw/*62.175*/("""class="form-control"  value=""""),_display_(/*62.205*/applicationUser/*62.220*/.getEmail()),format.raw/*62.231*/("""" autocomplete="off" type="email" required="required">
				</div> 
			</div>
			<div class="row">
				<div id="" class="col-md-6">
					<span class="glyphicon glyphicon-asterisk asterix"></span><label class="control-label" for="">Mot de passe:  </label>
					<input id="password" placeholder="mot de passer" name="password" """),_display_(/*68.72*/isDisabled("password")),format.raw/*68.94*/(""" """),format.raw/*68.95*/("""class="form-control" value=""""),_display_(/*68.124*/applicationUser/*68.139*/.getPassword()),format.raw/*68.153*/("""" autocomplete="off" type="password" required="required">
				</div> 
			</div>
			<div class="row">
				<div id="" class="col-md-6">
					<span class="glyphicon glyphicon-asterisk asterix"></span><label class="control-label" for="">Ville/Localité:  </label>
					<input  placeholder="Votre ville" name="ville" """),_display_(/*74.54*/isDisabled("ville")),format.raw/*74.73*/(""" """),format.raw/*74.74*/("""class="form-control" value=""""),_display_(/*74.103*/applicationUser/*74.118*/.getVille()),format.raw/*74.129*/("""" autocomplete="off" type="text" required="required">
				</div>
			</div>
			<div class="row">
				<div id="" class="col-md-6">
					<label class="control-label" for="">Pays:  </label>
					<input id="" placeholder="Votre natif" name="pays" """),_display_(/*80.58*/isDisabled("pays")),format.raw/*80.76*/(""" """),format.raw/*80.77*/("""class="form-control" value=""""),_display_(/*80.106*/applicationUser/*80.121*/.getPays()),format.raw/*80.131*/("""" autocomplete="off" type="text">
				</div>
			</div>
			<div class="row">
				<div id="" class="col-md-6">
					<span class="glyphicon glyphicon-asterisk asterix"></span><label class="control-label" for="">Téléphone:  </label>
					<input type="tel" placeholder="numéro de téléphone" name="tel" """),_display_(/*86.70*/isDisabled("tel")),format.raw/*86.87*/(""" """),format.raw/*86.88*/("""class="form-control" value=""""),_display_(/*86.117*/applicationUser/*86.132*/.getTel()),format.raw/*86.141*/("""" autocomplete="off"  required="required">
				</div>
			</div>
			<br>
			<div class="row">
				<div id="" class="col-md-6">
					<span class="glyphicon glyphicon-asterisk asterix"></span><label class="control-label" for="">Profil Utilisateur:  </label>
					<div class="multiselect">
						"""),_display_(/*94.8*/for(userProfil <- profilList) yield /*94.37*/{_display_(Seq[Any](format.raw/*94.38*/("""
							"""),_display_(/*95.9*/if(null != applicationUser.getMetaData && userProfilList.contains(userProfil))/*95.87*/{_display_(Seq[Any](format.raw/*95.88*/("""
								"""),format.raw/*96.9*/("""<p><input type="checkbox" name="profilUser" value=""""),_display_(/*96.61*/userProfil),format.raw/*96.71*/("""" class=""""),_display_(/*96.81*/userProfil),format.raw/*96.91*/("""" checked="checked"/> """),_display_(/*96.114*/userProfil),format.raw/*96.124*/("""</p>
							""")))}/*97.9*/else/*97.13*/{_display_(Seq[Any](format.raw/*97.14*/("""
								"""),format.raw/*98.9*/("""<p><input type="checkbox" name="profilUser" value=""""),_display_(/*98.61*/userProfil),format.raw/*98.71*/("""" class=""""),_display_(/*98.81*/userProfil),format.raw/*98.91*/("""" /> """),_display_(/*98.97*/userProfil),format.raw/*98.107*/("""</p>
							""")))}),format.raw/*99.9*/("""
						""")))}),format.raw/*100.8*/("""
					"""),format.raw/*101.6*/("""</div>
				</div> 
			</div>
			<br>
			<div class="form-inline">
				<div id="" class="form-group">
					<label class="control-label" for="">IS Actif</label>
					<input id="isActive" name="isActive" class="boolean-checkbox" style="width: 40px;" value="" """),_display_(/*108.99*/isDisabled("isActive")),format.raw/*108.121*/(""" """),_display_(/*108.123*/if(viewMode != "CREATE" && applicationUser.getIsActive())/*108.180*/ {_display_(Seq[Any](format.raw/*108.182*/("""checked="checked" """)))}),format.raw/*108.201*/(""" """),format.raw/*108.202*/("""type="checkbox">
				</div>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div class="form-group">
					"""),_display_(/*112.7*/if(viewMode == "CREATE")/*112.31*/ {_display_(Seq[Any](format.raw/*112.33*/("""
					"""),format.raw/*113.6*/("""<button type="submit" class="btn btn-primary text-right" data-rel="tooltip" title="Valider(Enregistrer les informations)">
						Valider
						<i class="fa fa-ok"></i></button>
					""")))}),format.raw/*116.7*/("""
					"""),_display_(/*117.7*/if(viewMode == "EDIT")/*117.29*/ {_display_(Seq[Any](format.raw/*117.31*/("""
					"""),format.raw/*118.6*/("""<button type="submit" class="btn btn-primary text-right" data-rel="tooltip" title="Valider(Modifier les informations)">
						Modifier
						<i class="fa fa-ok"></i></button>
					""")))}),format.raw/*121.7*/("""
				"""),format.raw/*122.5*/("""</div>
			</div>
		</div>
			<br/>
                            <table class="table table-bordered table-condensed">
                                <tbody>
                                	"""),_display_(/*128.35*/if(viewMode == "DELETE")/*128.59*/ {_display_(Seq[Any](format.raw/*128.61*/("""
                                     """),format.raw/*129.38*/("""<tr>
                                            <td colspan="2">
                                                <div id="feedBackArea" style="margin-bottom: 6px !important;" class="alert alert-danger">
                                                    <b>Attention :</b>
                                                    la suppression de cet utilisateur supprimera les enregistrements suivants:
                                                </div>
                                                    <b>-></b>
                                                    le(s) projet(s) engagement(s) qu'il a saisi ou validé
                                                     <br>
                                                     <b>-></b>
                                                    le(s) projet(s) liquidation(s) qu'il a saisi ou validé
                                                     <br>
                                                     <b>-></b>
                                                    le(s) mandat(s) qu'il a saisi ou validé
                                                     <br>
                                                    
                                            </td>
                                        </tr>
                                     """)))}),format.raw/*147.39*/("""
                                    """),format.raw/*148.37*/("""<tr>
                                        <td>
                                            """),_display_(/*150.46*/if(viewMode == "CREATE")/*150.70*/ {_display_(Seq[Any](format.raw/*150.72*/("""
                                                """),format.raw/*151.49*/("""<a href=""""),_display_(/*151.59*/routes/*151.65*/.ApplicationUserCtrl.list(0, "")),format.raw/*151.97*/("""" class="btn btn-success" data-rel="tooltip" title="liste des utilisateurs">
                                                   Retour<i class="fa fa-arrow-left"></i></a>
                                            """)))}),format.raw/*153.46*/("""
                                            """),_display_(/*154.46*/if(viewMode == "EDIT" || viewMode == "DELETE")/*154.92*/ {_display_(Seq[Any](format.raw/*154.94*/("""
                                                """),format.raw/*155.49*/("""<a href=""""),_display_(/*155.59*/routes/*155.65*/.ApplicationUserCtrl.list(0, "")),format.raw/*155.97*/("""" class="btn btn-warning" data-rel="tooltip" title="Abandonner la modification en cours">
                                                    Abandonner<i class="fa fa-arrow-left"></i></a>
                                            """)))}),format.raw/*157.46*/("""
                                        """),format.raw/*158.41*/("""</td>
                                        <td class="text-right">
                                            
                                            """),_display_(/*161.46*/if(viewMode == "EDIT")/*161.68*/ {_display_(Seq[Any](format.raw/*161.70*/("""
                                                """),format.raw/*162.49*/("""<a href=""""),_display_(/*162.59*/routes/*162.65*/.ApplicationUserCtrl.delete(applicationUser.getId())),format.raw/*162.117*/("""" class="btn btn-danger text-right" data-rel="tooltip" title="Confirmer la suppresion">
                                                    Supprimer
                                                    <i class="fa fa-ok"></i>
                                                </a>

                                            """)))}),format.raw/*167.46*/("""

                                        """),format.raw/*169.41*/("""</td>
                                    </tr>
                                </tbody>
                            </table>
                  """)))}),format.raw/*173.20*/("""
	    
   """),format.raw/*175.4*/("""</div>
</div>


""")))}),format.raw/*179.2*/("""

"""),format.raw/*181.1*/("""<script>
function ckChange(ckType)"""),format.raw/*182.26*/("""{"""),format.raw/*182.27*/("""
    """),format.raw/*183.5*/("""var ckName = document.getElementsByName(ckType.name);
    var checked = document.getElementById(ckType.id);

    if (checked.checked) """),format.raw/*186.26*/("""{"""),format.raw/*186.27*/("""
      """),format.raw/*187.7*/("""for(var i=0; i < ckName.length; i++)"""),format.raw/*187.43*/("""{"""),format.raw/*187.44*/("""

          """),format.raw/*189.11*/("""if(!ckName[i].checked)"""),format.raw/*189.33*/("""{"""),format.raw/*189.34*/("""
              """),format.raw/*190.15*/("""ckName[i].disabled = true;
          """),format.raw/*191.11*/("""}"""),format.raw/*191.12*/("""else"""),format.raw/*191.16*/("""{"""),format.raw/*191.17*/("""
              """),format.raw/*192.15*/("""ckName[i].disabled = false;
          """),format.raw/*193.11*/("""}"""),format.raw/*193.12*/("""
      """),format.raw/*194.7*/("""}"""),format.raw/*194.8*/("""
    """),format.raw/*195.5*/("""}"""),format.raw/*195.6*/("""
    """),format.raw/*196.5*/("""else """),format.raw/*196.10*/("""{"""),format.raw/*196.11*/("""
      """),format.raw/*197.7*/("""for(var i=0; i < ckName.length; i++)"""),format.raw/*197.43*/("""{"""),format.raw/*197.44*/("""
        """),format.raw/*198.9*/("""ckName[i].disabled = false;
      """),format.raw/*199.7*/("""}"""),format.raw/*199.8*/("""
    """),format.raw/*200.5*/("""}"""),format.raw/*200.6*/("""
"""),format.raw/*201.1*/("""}"""),format.raw/*201.2*/("""
"""),format.raw/*202.1*/("""</script>

"""))
      }
    }
  }

  def render(applicationUser:models.tables.pojos.ApplicationUser,viewMode:String,profil:String,profilList:List[String],userProfilList:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(applicationUser,viewMode,profil,profilList,userProfilList)

  def f:((models.tables.pojos.ApplicationUser,String,String,List[String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (applicationUser,viewMode,profil,profilList,userProfilList) => apply(applicationUser,viewMode,profil,profilList,userProfilList)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 10:28:43 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/applicationUserShow.scala.html
                  HASH: f7356212f5347e31ba877e8651c846dccec08ffa
                  MATRIX: 1031->1|1261->619|1295->646|1324->161|1337->166|1385->202|1404->212|1837->157|1867->200|1895->616|1924->663|2155->884|2196->886|2225->888|2379->1016|2412->1040|2451->1041|2481->1044|2547->1092|2583->1119|2622->1120|2652->1123|2714->1167|2750->1194|2789->1195|2819->1198|2903->1252|2936->1259|2984->1298|3023->1299|3053->1303|3068->1309|3104->1324|3135->1328|3207->1373|3236->1381|3308->1426|3332->1441|3359->1447|3665->1726|3703->1743|3732->1744|3790->1774|3815->1789|3846->1798|4186->2111|4227->2131|4256->2132|4314->2162|4339->2177|4370->2186|4743->2533|4784->2552|4814->2554|4858->2576|4888->2578|4920->2600|4960->2601|5012->2621|5042->2622|5100->2652|5125->2667|5158->2678|5510->3003|5553->3025|5582->3026|5639->3055|5664->3070|5700->3084|6039->3396|6079->3415|6108->3416|6165->3445|6190->3460|6223->3471|6493->3714|6532->3732|6561->3733|6618->3762|6643->3777|6675->3787|7000->4085|7038->4102|7067->4103|7124->4132|7149->4147|7180->4156|7499->4449|7544->4478|7583->4479|7618->4488|7705->4566|7744->4567|7780->4576|7859->4628|7890->4638|7927->4648|7958->4658|8009->4681|8041->4691|8072->4704|8085->4708|8124->4709|8160->4718|8239->4770|8270->4780|8307->4790|8338->4800|8371->4806|8403->4816|8446->4829|8485->4837|8519->4843|8804->5100|8849->5122|8880->5124|8948->5181|8990->5183|9042->5202|9073->5203|9240->5343|9274->5367|9315->5369|9349->5375|9563->5558|9597->5565|9629->5587|9670->5589|9704->5595|9916->5776|9949->5781|10167->5971|10201->5995|10242->5997|10309->6035|11652->7346|11718->7383|11841->7478|11875->7502|11916->7504|11994->7553|12032->7563|12048->7569|12102->7601|12350->7817|12424->7863|12480->7909|12521->7911|12599->7960|12637->7970|12653->7976|12707->8008|12973->8242|13043->8283|13231->8443|13263->8465|13304->8467|13382->8516|13420->8526|13436->8532|13511->8584|13869->8910|13940->8952|14117->9097|14155->9107|14203->9124|14233->9126|14296->9160|14326->9161|14359->9166|14522->9300|14552->9301|14587->9308|14652->9344|14682->9345|14723->9357|14774->9379|14804->9380|14848->9395|14914->9432|14944->9433|14977->9437|15007->9438|15051->9453|15118->9491|15148->9492|15183->9499|15212->9500|15245->9505|15274->9506|15307->9511|15341->9516|15371->9517|15406->9524|15471->9560|15501->9561|15538->9570|15600->9604|15629->9605|15662->9610|15691->9611|15720->9612|15749->9613|15778->9614
                  LINES: 28->1|37->23|38->24|40->10|40->10|42->13|42->13|51->7|54->12|55->21|57->25|57->25|57->25|59->27|63->31|63->31|63->31|64->32|66->34|66->34|66->34|67->35|69->37|69->37|69->37|70->38|72->40|73->41|73->41|73->41|74->42|74->42|74->42|75->43|75->43|75->43|76->44|76->44|76->44|82->50|82->50|82->50|82->50|82->50|82->50|88->56|88->56|88->56|88->56|88->56|88->56|94->62|94->62|94->62|94->62|94->62|94->62|94->62|94->62|94->62|94->62|94->62|94->62|100->68|100->68|100->68|100->68|100->68|100->68|106->74|106->74|106->74|106->74|106->74|106->74|112->80|112->80|112->80|112->80|112->80|112->80|118->86|118->86|118->86|118->86|118->86|118->86|126->94|126->94|126->94|127->95|127->95|127->95|128->96|128->96|128->96|128->96|128->96|128->96|128->96|129->97|129->97|129->97|130->98|130->98|130->98|130->98|130->98|130->98|130->98|131->99|132->100|133->101|140->108|140->108|140->108|140->108|140->108|140->108|140->108|144->112|144->112|144->112|145->113|148->116|149->117|149->117|149->117|150->118|153->121|154->122|160->128|160->128|160->128|161->129|179->147|180->148|182->150|182->150|182->150|183->151|183->151|183->151|183->151|185->153|186->154|186->154|186->154|187->155|187->155|187->155|187->155|189->157|190->158|193->161|193->161|193->161|194->162|194->162|194->162|194->162|199->167|201->169|205->173|207->175|211->179|213->181|214->182|214->182|215->183|218->186|218->186|219->187|219->187|219->187|221->189|221->189|221->189|222->190|223->191|223->191|223->191|223->191|224->192|225->193|225->193|226->194|226->194|227->195|227->195|228->196|228->196|228->196|229->197|229->197|229->197|230->198|231->199|231->199|232->200|232->200|233->201|233->201|234->202
                  -- GENERATED --
              */
          