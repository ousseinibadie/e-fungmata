
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object changePassword extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(profil: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.2*/import helper._


Seq[Any](format.raw/*1.18*/("""
"""),_display_(/*3.2*/main("Changement de mot passe")/*3.33*/{_display_(Seq[Any](format.raw/*3.34*/("""
"""),format.raw/*4.1*/("""<div style="margin-right: -80px;margin-left: 24px;">
<div class="container">
	<div class="row">
		<label class="label label-info" style="float:right;">"""),_display_(/*7.57*/profil),format.raw/*7.63*/("""</label>
		<br>
		 <h5>
			 <div class="badged-title">Changer votre mot de passe</div>
			 <br>
	    </h5>
	    """),_display_(/*13.7*/form(routes.ApplicationUserCtrl.changePassword())/*13.56*/{_display_(Seq[Any](format.raw/*13.57*/("""
		"""),_display_(/*14.4*/helper/*14.10*/.CSRF.formField),format.raw/*14.25*/("""
	    """),format.raw/*15.6*/("""<div class="dataform">
	    	<br />
			<div class="form-inline">
				<div id="nameUser" class="form-group">
					<label class="control-label" for="">Ancien mot de passe:  </label>
					<input id="name" name="password"  class="form-control" style="width: 90%;" value="" autocomplete="off" type="password" required="required">
				</div> 
			</div>
			<div class="form-inline">
				<div id="emailUser" class="form-group">
					<label class="control-label" for="">Nouveau mot de passe:  </label>
					<input id="nouveauPassword" name="nouveauPassword" class="form-control" style="width: 90%;" value="" autocomplete="off" type="password" required="required">
				</div> 
			</div>
			<div class="form-inline">
				<div id="passwordUser" class="form-group">
					<label class="control-label" for="">Confirmer mot de passe:  </label>
					<input id="confirmePassword" name="confirmePassword" class="form-control" style="width: 90%;" value="" autocomplete="off" type="password" required="required">
				</div> 
			</div>
			<br />
			<div class="form-inline">
				<div class="form-group" style="text-align: right;">
                   	 	<button type="submit" class="btn btn-primary text-right" data-rel="tooltip" title="Valider(Enregistrer les informations)">
                     	Changer votre mot de passe
            			<i class="fa fa-ok"></i></button>
				</div>
			</div>	
		</div>
			
      """)))}),format.raw/*45.8*/("""
	    
   """),format.raw/*47.4*/("""</div>
</div>
</div>

""")))}))
      }
    }
  }

  def render(profil:String): play.twirl.api.HtmlFormat.Appendable = apply(profil)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (profil) => apply(profil)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 10:28:43 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/changePassword.scala.html
                  HASH: 6259620bac0b95e7a00e065ec8f39493c28a3b92
                  MATRIX: 957->1|1046->19|1091->17|1118->36|1157->67|1195->68|1222->69|1400->221|1426->227|1565->340|1623->389|1662->390|1692->394|1707->400|1743->415|1776->421|3200->1815|3237->1825
                  LINES: 28->1|31->2|34->1|35->3|35->3|35->3|36->4|39->7|39->7|45->13|45->13|45->13|46->14|46->14|46->14|47->15|77->45|79->47
                  -- GENERATED --
              */
          