
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object projetInvestissement extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[models.tables.pojos.VProjetAttente],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(projetAttenteList: List[models.tables.pojos.VProjetAttente]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.2*/import controllers.util.Helpers.getCurrentUser
/*3.2*/import controllers.util.Helpers.isCurrentUserInvestissement
/*5.2*/import views.html.utils._


Seq[Any](format.raw/*1.63*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*6.2*/main("Investir")/*6.18*/{_display_(Seq[Any](format.raw/*6.19*/("""
    """),format.raw/*7.5*/("""<h3 class="text-center">Les projets encours de financement</h3>
    """),_display_(/*8.6*/for(projet <- projetAttenteList) yield /*8.38*/{_display_(Seq[Any](format.raw/*8.39*/("""

    """),format.raw/*10.5*/("""<div class="col-md-3">
        <div class="card-deck">
            <div class="card">
                <img class="card-img-top" src='"""),_display_(/*13.49*/routes/*13.55*/.Assets.versioned("images/image-projet.jpg")),format.raw/*13.99*/("""' alt="">
                <div class="card-body">
                    <h5 class="card-title"><b>Secteur d'activité:</b> """),_display_(/*15.72*/projet/*15.78*/.getIntituleSecteurActivite),format.raw/*15.105*/("""</h5>
                    <p class="card-text"><b>Intitulé du projet:</b> """),_display_(/*16.70*/projet/*16.76*/.getNom()),format.raw/*16.85*/("""</p>
                    <p class="card-text">
                        <small class="text-muted">
                            <span class="text-danger">Côut du projet: """),_display_(/*19.72*/formatMontant(projet.getMontantProjet)),format.raw/*19.110*/(""" """),format.raw/*19.111*/("""FCFA</span><br>
                            <span class="text-primary"><font size="2"><b>Montant encaissé:</b></font> """),_display_(/*20.104*/formatMontant(projet.getMontantEncaisse)),format.raw/*20.144*/(""" """),format.raw/*20.145*/("""FCFA</span><br>
                            <a href=""""),_display_(/*21.39*/routes/*21.45*/.ProjetAttenteCtrl.show("VIEW",projet.getId)),format.raw/*21.89*/("""" class="btn btn-primary btn-sm">Voir +</a>
                            """),_display_(/*22.30*/if(isCurrentUserInvestissement)/*22.61*/{_display_(Seq[Any](format.raw/*22.62*/("""
                                """),format.raw/*23.33*/("""<button data-toggle="modal" data-target="#myModal" class="btn btn-success btn-sm">Financer maintenant</button>
                            """)))}),format.raw/*24.30*/("""
                        """),format.raw/*25.25*/("""</small>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <form class="cmxform form-horizontal tasi-form" method="POST" action=""""),_display_(/*32.76*/routes/*32.82*/.ProjetAttenteCtrl.investir()),format.raw/*32.111*/("""" accept-charset="UTF-8">
        """),_display_(/*33.10*/helper/*33.16*/.CSRF.formField),format.raw/*33.31*/("""
        """),format.raw/*34.9*/("""<input type="hidden" name="projetId" value=""""),_display_(/*34.54*/projet/*34.60*/.getId),format.raw/*34.66*/("""" va />
        <input type="hidden" name="investisseur" value=""""),_display_(/*35.58*/getCurrentUser()/*35.74*/.getId),format.raw/*35.80*/("""" />
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"></button>
                        <h4 class="center-left">Entrer votre contribution</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Montant: </label>
                                <input type=""  name="montant" class="form-control autonumeric" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button  type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                        <button id="button" class="btn btn-primary"> Valider</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    """)))}),format.raw/*62.6*/("""



""")))}))
      }
    }
  }

  def render(projetAttenteList:List[models.tables.pojos.VProjetAttente]): play.twirl.api.HtmlFormat.Appendable = apply(projetAttenteList)

  def f:((List[models.tables.pojos.VProjetAttente]) => play.twirl.api.HtmlFormat.Appendable) = (projetAttenteList) => apply(projetAttenteList)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 13:30:46 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/projetInvestissement.scala.html
                  HASH: 3a5b5c7e9aa77e4ed6257a5400c3faa3e7937426
                  MATRIX: 997->1|1131->64|1185->112|1252->174|1307->62|1334->172|1361->201|1385->217|1423->218|1454->223|1548->292|1595->324|1633->325|1666->331|1827->465|1842->471|1907->515|2055->636|2070->642|2119->669|2221->744|2236->750|2266->759|2462->928|2522->966|2552->967|2699->1086|2761->1126|2791->1127|2872->1181|2887->1187|2952->1231|3052->1304|3092->1335|3131->1336|3192->1369|3363->1509|3416->1534|3621->1712|3636->1718|3687->1747|3749->1782|3764->1788|3800->1803|3836->1812|3908->1857|3923->1863|3950->1869|4042->1934|4067->1950|4094->1956|5304->3136
                  LINES: 28->1|31->2|32->3|33->5|36->1|37->4|38->6|38->6|38->6|39->7|40->8|40->8|40->8|42->10|45->13|45->13|45->13|47->15|47->15|47->15|48->16|48->16|48->16|51->19|51->19|51->19|52->20|52->20|52->20|53->21|53->21|53->21|54->22|54->22|54->22|55->23|56->24|57->25|64->32|64->32|64->32|65->33|65->33|65->33|66->34|66->34|66->34|66->34|67->35|67->35|67->35|94->62
                  -- GENERATED --
              */
          