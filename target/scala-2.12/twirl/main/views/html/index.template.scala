
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.4*/("""

"""),_display_(/*3.2*/main("E-Griot")/*3.17*/ {_display_(Seq[Any](format.raw/*3.19*/("""
"""),format.raw/*4.1*/("""<div class="col-md-12">
    <div class="jumbotron">
  <h3 class="text-center">Investir dans l'automisation des femmes</h3>
     <p class="lead">
          <div id="carousel-home" class="owl-carousel text-white hidden-sm owl-theme" style="opacity: 1; display: block;">
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                      <li data-target="#myCarousel" data-slide-to="1"></li>
                      <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                      <div class="item active">
                          <img src='"""),_display_(/*20.38*/routes/*20.44*/.Assets.versioned("images/image1.jpg")),format.raw/*20.82*/("""' alt="aucune image">
                      </div>

                      <div class="item">
                          <img src='"""),_display_(/*24.38*/routes/*24.44*/.Assets.versioned("images/image2.jpg")),format.raw/*24.82*/("""' alt="aucune image">
                      </div>

                      <div class="item">
                          <img src='"""),_display_(/*28.38*/routes/*28.44*/.Assets.versioned("images/image3.jpg")),format.raw/*28.82*/("""' alt="New York">
                      </div>

                      <div class="item">
                          <img src='"""),_display_(/*32.38*/routes/*32.44*/.Assets.versioned("images/image4.jpg")),format.raw/*32.82*/("""' alt="New York">
                      </div>

                      <div class="item">
                          <img src='"""),_display_(/*36.38*/routes/*36.44*/.Assets.versioned("images/image5.jpg")),format.raw/*36.82*/("""' alt="New York">
                      </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                      <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                      <span class="sr-only">Next</span>
                  </a>
              </div>
          </div>
    </p>
        <hr class="my-4">
        <p class="lead">
            les plateformes de crowdfunding sont spécialisées dans le financement alternatif. Grâce à elles, les entrepreneurs ou créatifs soumettent leur projet à une communauté d'internautes dans le cadre d'une campagne de collecte de fonds. Les plateformes de crowdfunding fonctionnent comme des intermédiaires entre porteurs de projets et investisseurs particuliers. Chacun est libre d'apporter sa contribution dans le projet de son choix, à la hauteur de ses moyens. C'est l'association d'un grand nombre d'investisseurs qui permet au porteur de projet de financer son activité, selon le préceptes "les petits ruisseaux font les grandes rivières".
        </p>
    </div>
    </div>


""")))}),format.raw/*60.2*/("""
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 10:28:43 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/index.scala.html
                  HASH: fc9d0af5f148731831bc7562f55c0643bbfce582
                  MATRIX: 941->1|1037->3|1065->6|1088->21|1127->23|1154->24|2064->907|2079->913|2138->951|2295->1081|2310->1087|2369->1125|2526->1255|2541->1261|2600->1299|2753->1425|2768->1431|2827->1469|2980->1595|2995->1601|3054->1639|4493->3048
                  LINES: 28->1|33->1|35->3|35->3|35->3|36->4|52->20|52->20|52->20|56->24|56->24|56->24|60->28|60->28|60->28|64->32|64->32|64->32|68->36|68->36|68->36|92->60
                  -- GENERATED --
              */
          