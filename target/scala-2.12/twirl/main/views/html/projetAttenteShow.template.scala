
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object projetAttenteShow extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template4[models.tables.pojos.ProjetAtttente,String,Map[String, String],Map[String, String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(
    projetAttente : models.tables.pojos.ProjetAtttente,
    viewMode : String,
    typeProjet: Map[String, String],
    typeFinancement: Map[String, String]
):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*22.2*/import helper._

def /*8.2*/quote/*8.7*/(s: String) = {{
"" + s + ""
}};def /*12.2*/isDisabled/*12.12*/(fieldName: String) = {{
    fieldName match {
    case "projetAttente" => if(null == projetAttente.getId)
    "name=" + quote(fieldName) else "name=" + quote(fieldName) + " disabled=" + quote("disabled")
    case _ => if((viewMode != "VIEW") && (viewMode != "DELETE"))
    "name=" + quote(fieldName) else "name=" + quote(fieldName) + " disabled=" + quote("disabled")

    }
}};
Seq[Any](format.raw/*6.2*/("""

"""),format.raw/*10.2*/("""

"""),format.raw/*20.2*/("""

"""),_display_(/*23.2*/main(if(viewMode == "CREATE") "Nouveau projet" else if(viewMode == "VIEW") "Consultation projet" else if(viewMode == "EDIT") "Edition projet" else if(viewMode == "DELETE") "Suppression projet" else "")/*23.203*/ {_display_(Seq[Any](format.raw/*23.205*/("""

"""),format.raw/*25.1*/("""<br><br>
<div class="panel panel-default">
    <label class="label label-info" style="float: right;"></label>

    """),_display_(/*29.6*/if(viewMode == "CREATE")/*29.30*/{_display_(Seq[Any](format.raw/*29.31*/("""
    """),format.raw/*30.5*/("""<div class="badged-title">Soumission d'un nouveau projet</div>

    """)))}/*32.6*/else if(viewMode == "EDIT")/*32.33*/{_display_(Seq[Any](format.raw/*32.34*/("""
    """),format.raw/*33.5*/("""<div class="badged-title">Edition d'un projet</div>

    """)))}/*35.6*/else if(viewMode == "VIEW")/*35.33*/{_display_(Seq[Any](format.raw/*35.34*/("""
    """),format.raw/*36.5*/("""<div class="badged-title">Détails projets</div>

    """)))}),format.raw/*38.6*/("""
    """),format.raw/*39.5*/("""<form id="projetForm" action=""""),_display_(/*39.36*/routes/*39.42*/.ProjetAttenteCtrl.saveOrUpdate()),format.raw/*39.75*/("""" class=" " method="post" eenctype="multipart/form-data">
        """),_display_(/*40.10*/CSRF/*40.14*/.formField),format.raw/*40.24*/("""
        """),format.raw/*41.9*/("""<input type="hidden" class="form-control" name="id" value=""""),_display_(/*41.69*/projetAttente/*41.82*/.getId()),format.raw/*41.90*/(""""/>
        <input type="hidden" class="form-control" name="viewMode" value=""""),_display_(/*42.75*/viewMode),format.raw/*42.83*/("""">
        <div class="panel-body">
            <!--premiere ligne-->
                    <div class="prog_cat">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="glyphicon glyphicon-asterisk asterix"></span>&nbsp;<label class="control-label">Intitulé du projet</label>
                                <input type="text" class="form-control" name="nom" value=""""),_display_(/*49.92*/projetAttente/*49.105*/.getNom),format.raw/*49.112*/(""""  required="required" autocomplete="off" placeholder="intitule de votre projet""""),_display_(/*49.193*/isDisabled("intitutle")),format.raw/*49.216*/(""" """),format.raw/*49.217*/("""/>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="glyphicon glyphicon-asterisk asterix"></span>&nbsp;<label class="control-label">Type du projet</label>
                                <select class="form-control" name="typeProjet"  required="required" """),_display_(/*57.102*/isDisabled("typeProjet")),format.raw/*57.126*/(""">
                                    <option value="">-- Choisir type projet --</option>
                                    """),_display_(/*59.38*/for((key,value) <- typeProjet) yield /*59.68*/{_display_(Seq[Any](format.raw/*59.69*/("""
                                        """),_display_(/*60.42*/if(projetAttente.getTypeProjet != null && projetAttente.getTypeProjet.equals(key))/*60.124*/{_display_(Seq[Any](format.raw/*60.125*/("""
                                            """),format.raw/*61.45*/("""<option value=""""),_display_(/*61.61*/key),format.raw/*61.64*/("""" selected="selected">"""),_display_(/*61.87*/value),format.raw/*61.92*/("""</option>
                                        """)))}/*62.42*/else/*62.46*/{_display_(Seq[Any](format.raw/*62.47*/("""
                                            """),format.raw/*63.45*/("""<option value=""""),_display_(/*63.61*/key),format.raw/*63.64*/("""">"""),_display_(/*63.67*/value),format.raw/*63.72*/("""</option>
                                        """)))}),format.raw/*64.42*/("""
                                    """)))}),format.raw/*65.38*/("""

                                """),format.raw/*67.33*/("""</select>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                    <span class="glyphicon glyphicon-asterisk asterix"></span>&nbsp;<label class="control-label">Secteur activité</label>
                                <select class="form-control" name="secteurActivite"  required="required"  """),_display_(/*75.108*/isDisabled("secteurActivite")),format.raw/*75.137*/(""">
                                    <option value="">-- Choisir un secteur activité --</option>
                                    """),_display_(/*77.38*/for((key,value) <- typeFinancement) yield /*77.73*/{_display_(Seq[Any](format.raw/*77.74*/("""
                                        """),_display_(/*78.42*/if(projetAttente.getSecteurActivite != null && projetAttente.getSecteurActivite.equals(key))/*78.134*/{_display_(Seq[Any](format.raw/*78.135*/("""
                                            """),format.raw/*79.45*/("""<option value=""""),_display_(/*79.61*/key),format.raw/*79.64*/("""" selected="selected">"""),_display_(/*79.87*/value),format.raw/*79.92*/("""</option>
                                        """)))}/*80.42*/else/*80.46*/{_display_(Seq[Any](format.raw/*80.47*/("""
                                            """),format.raw/*81.45*/("""<option value=""""),_display_(/*81.61*/key),format.raw/*81.64*/("""">"""),_display_(/*81.67*/value),format.raw/*81.72*/("""</option>
                                        """)))}),format.raw/*82.42*/("""
                                    """)))}),format.raw/*83.38*/("""

                                """),format.raw/*85.33*/("""</select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="glyphicon glyphicon-asterisk asterix"></span>&nbsp;<label class="control-label">Description du projet</label>
                                <textarea rows="6" type="text" maxlength="500" class="form-control" name="description"  required="required"  placeholder="texte de description ne doit pas depasser 500 mots" """),_display_(/*92.208*/isDisabled("description")),format.raw/*92.233*/("""> """),_display_(/*92.236*/projetAttente/*92.249*/.getDescription),format.raw/*92.264*/("""</textarea>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="glyphicon glyphicon-asterisk asterix"></span>&nbsp;<label class="control-label">Montant total (FCFA)</label>
                                <input type="" class="form-control autonumeric" name="montant"  required="required" placeholder="montant total du projet" value=""""),_display_(/*100.163*/projetAttente/*100.176*/.getMontant),format.raw/*100.187*/("""" autocomplete="off" """),_display_(/*100.209*/isDisabled("montant")),format.raw/*100.230*/("""/>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="glyphicon glyphicon-asterisk asterix"></span>&nbsp;<label class="control-label">Document à fournir</label>
                                """),_display_(/*107.34*/if(viewMode == "CREATE" || viewMode == "EDIT")/*107.80*/{_display_(Seq[Any](format.raw/*107.81*/("""
                                    """),format.raw/*108.37*/("""<input type="file" class="form-control" name="documentFournit"  required="required" placeholder="type de financement" value=""""),_display_(/*108.163*/projetAttente/*108.176*/.getDocumentFournit),format.raw/*108.195*/(""""  """),_display_(/*108.199*/isDisabled("documentFournit")),format.raw/*108.228*/(""" """),format.raw/*108.229*/("""/>
                                """)))}/*109.34*/else/*109.38*/{_display_(Seq[Any](format.raw/*109.39*/("""
                                    """),format.raw/*110.37*/("""<input type="text" class="form-control"   required="required" placeholder="type de financement" value=""""),_display_(/*110.141*/projetAttente/*110.154*/.getDocumentFournit),format.raw/*110.173*/(""""  """),_display_(/*110.177*/isDisabled("documentFournit")),format.raw/*110.206*/(""" """),format.raw/*110.207*/("""/>
                                """)))}),format.raw/*111.34*/("""
                            """),format.raw/*112.29*/("""</div>
                        </div>
                        <br>

                    </div>


            <br />
            <!--8eme ligne-->
            <div class="form-group text-center">
                <a  class="btn btn-default" href="/"><i class="fa fa-arrow-left"></i> Retour</a>&nbsp;
                """),_display_(/*123.18*/if(viewMode == "CREATE")/*123.42*/{_display_(Seq[Any](format.raw/*123.43*/("""
                    """),format.raw/*124.21*/("""<button type="submit" class="btn btn-primary" id="saveButton">Enregistrer <i class="fa fa-check"></i></button>
                """)))}),format.raw/*125.18*/("""
                """),_display_(/*126.18*/if(viewMode == "EDIT")/*126.40*/{_display_(Seq[Any](format.raw/*126.41*/("""
                """),format.raw/*127.17*/("""<button type="submit" class="btn btn-primary" id="editButton">Modifier <i class="fa fa-check"></i></button>
                """)))}),format.raw/*128.18*/("""
            """),format.raw/*129.13*/("""</div>
        </div>
        </div>
    </form>

</div>
</div>


""")))}),format.raw/*138.2*/("""

"""),format.raw/*140.1*/("""<script>
$(function()"""),format.raw/*141.13*/("""{"""),format.raw/*141.14*/("""

       """),format.raw/*143.8*/("""/*--montantavec separateur de millier--------------------*/
    	$('.separateur-millier').on('change',function()"""),format.raw/*144.53*/("""{"""),format.raw/*144.54*/("""
    						"""),format.raw/*145.11*/("""console.log('Change event.');
    						var val = $('.separateur-millier').val();

    	"""),format.raw/*148.6*/("""}"""),format.raw/*148.7*/(""");

        $('.separateur-millier').change(function()"""),format.raw/*150.51*/("""{"""),format.raw/*150.52*/("""
    		"""),format.raw/*151.7*/("""console.log('Second change event...');
    	"""),format.raw/*152.6*/("""}"""),format.raw/*152.7*/(""");

    	$('.separateur-millier').number( true, 0, ',', ' ' );


"""),format.raw/*157.1*/("""}"""),format.raw/*157.2*/(""");
</script>
"""))
      }
    }
  }

  def render(projetAttente:models.tables.pojos.ProjetAtttente,viewMode:String,typeProjet:Map[String, String],typeFinancement:Map[String, String]): play.twirl.api.HtmlFormat.Appendable = apply(projetAttente,viewMode,typeProjet,typeFinancement)

  def f:((models.tables.pojos.ProjetAtttente,String,Map[String, String],Map[String, String]) => play.twirl.api.HtmlFormat.Appendable) = (projetAttente,viewMode,typeProjet,typeFinancement) => apply(projetAttente,viewMode,typeProjet,typeFinancement)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 11:47:39 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/projetAttenteShow.scala.html
                  HASH: 0aa0d4ce603d70e1dd84635844e4b947ad880f2c
                  MATRIX: 1035->1|1269->591|1297->164|1309->169|1353->202|1372->212|1777->161|1806->199|1835->588|1864->608|2075->809|2116->811|2145->813|2287->929|2320->953|2359->954|2391->959|2478->1028|2514->1055|2553->1056|2585->1061|2661->1119|2697->1146|2736->1147|2768->1152|2852->1206|2884->1211|2942->1242|2957->1248|3011->1281|3105->1348|3118->1352|3149->1362|3185->1371|3272->1431|3294->1444|3323->1452|3428->1530|3457->1538|3933->1987|3956->2000|3985->2007|4094->2088|4139->2111|4169->2112|4638->2553|4684->2577|4838->2704|4884->2734|4923->2735|4992->2777|5084->2859|5124->2860|5197->2905|5240->2921|5264->2924|5314->2947|5340->2952|5410->3003|5423->3007|5462->3008|5535->3053|5578->3069|5602->3072|5632->3075|5658->3080|5740->3131|5809->3169|5871->3203|6359->3663|6410->3692|6572->3827|6623->3862|6662->3863|6731->3905|6833->3997|6873->3998|6946->4043|6989->4059|7013->4062|7063->4085|7089->4090|7159->4141|7172->4145|7211->4146|7284->4191|7327->4207|7351->4210|7381->4213|7407->4218|7489->4269|7558->4307|7620->4341|8208->4901|8255->4926|8286->4929|8309->4942|8346->4957|8892->5474|8916->5487|8950->5498|9001->5520|9045->5541|9449->5917|9505->5963|9545->5964|9611->6001|9766->6127|9790->6140|9832->6159|9865->6163|9917->6192|9948->6193|10004->6229|10018->6233|10058->6234|10124->6271|10257->6375|10281->6388|10323->6407|10356->6411|10408->6440|10439->6441|10507->6477|10565->6506|10908->6821|10942->6845|10982->6846|11032->6867|11192->6995|11238->7013|11270->7035|11310->7036|11356->7053|11513->7178|11555->7191|11653->7258|11683->7260|11733->7281|11763->7282|11800->7291|11941->7403|11971->7404|12011->7415|12127->7503|12156->7504|12239->7558|12269->7559|12304->7566|12376->7610|12405->7611|12498->7676|12527->7677
                  LINES: 28->1|36->22|38->8|38->8|40->12|40->12|49->6|51->10|53->20|55->23|55->23|55->23|57->25|61->29|61->29|61->29|62->30|64->32|64->32|64->32|65->33|67->35|67->35|67->35|68->36|70->38|71->39|71->39|71->39|71->39|72->40|72->40|72->40|73->41|73->41|73->41|73->41|74->42|74->42|81->49|81->49|81->49|81->49|81->49|81->49|89->57|89->57|91->59|91->59|91->59|92->60|92->60|92->60|93->61|93->61|93->61|93->61|93->61|94->62|94->62|94->62|95->63|95->63|95->63|95->63|95->63|96->64|97->65|99->67|107->75|107->75|109->77|109->77|109->77|110->78|110->78|110->78|111->79|111->79|111->79|111->79|111->79|112->80|112->80|112->80|113->81|113->81|113->81|113->81|113->81|114->82|115->83|117->85|124->92|124->92|124->92|124->92|124->92|132->100|132->100|132->100|132->100|132->100|139->107|139->107|139->107|140->108|140->108|140->108|140->108|140->108|140->108|140->108|141->109|141->109|141->109|142->110|142->110|142->110|142->110|142->110|142->110|142->110|143->111|144->112|155->123|155->123|155->123|156->124|157->125|158->126|158->126|158->126|159->127|160->128|161->129|170->138|172->140|173->141|173->141|175->143|176->144|176->144|177->145|180->148|180->148|182->150|182->150|183->151|184->152|184->152|189->157|189->157
                  -- GENERATED --
              */
          