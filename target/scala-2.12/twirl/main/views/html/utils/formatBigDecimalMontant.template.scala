
package views.html.utils

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object formatBigDecimalMontant extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[BigDecimal,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(montant: BigDecimal):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.23*/("""

    """),_display_(/*3.6*/{
        if(null != montant) {
            val locale = new java.util.Locale("fr", "FR")
            val formatter = java.text.NumberFormat.getIntegerInstance(locale)
            formatter.format(montant)
        }
    }),format.raw/*9.6*/("""
"""))
      }
    }
  }

  def render(montant:BigDecimal): play.twirl.api.HtmlFormat.Appendable = apply(montant)

  def f:((BigDecimal) => play.twirl.api.HtmlFormat.Appendable) = (montant) => apply(montant)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 10:28:44 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/utils/formatBigDecimalMontant.scala.html
                  HASH: 9aa570e145f0e2fc01432be3c40745c451eff8e9
                  MATRIX: 976->1|1092->22|1124->29|1364->250
                  LINES: 28->1|33->1|35->3|41->9
                  -- GENERATED --
              */
          