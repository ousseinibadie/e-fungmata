
package views.html.utils

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object formatMontant extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Long,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(montant: Long):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.17*/("""

    """),_display_(/*3.6*/{
        if(null != montant) {
            val locale = new java.util.Locale("fr", "FR")
            val formatter = java.text.NumberFormat.getIntegerInstance(locale)
            formatter.format(montant)
        }
    }),format.raw/*9.6*/("""
"""))
      }
    }
  }

  def render(montant:Long): play.twirl.api.HtmlFormat.Appendable = apply(montant)

  def f:((Long) => play.twirl.api.HtmlFormat.Appendable) = (montant) => apply(montant)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 10:28:44 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/utils/formatMontant.scala.html
                  HASH: 83a8cb473568fff7fe5db460a1b61ef9f7558bb2
                  MATRIX: 960->1|1070->16|1102->23|1342->244
                  LINES: 28->1|33->1|35->3|41->9
                  -- GENERATED --
              */
          