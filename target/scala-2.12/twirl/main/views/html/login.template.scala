
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object login extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.4*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Login</title>
	<link rel="stylesheet" href='"""),_display_(/*9.32*/routes/*9.38*/.Assets.versioned("css/bootstrap.css")),format.raw/*9.76*/("""'>
	<link rel="stylesheet" href='"""),_display_(/*10.32*/routes/*10.38*/.Assets.versioned("css/login.css")),format.raw/*10.72*/("""'>

	<style type="text/css">
.center """),format.raw/*13.9*/("""{"""),format.raw/*13.10*/("""
  """),format.raw/*14.3*/("""display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
"""),format.raw/*18.1*/("""}"""),format.raw/*18.2*/("""
"""),format.raw/*19.1*/("""</style>
</head>
<body>
<div class="wrapper">
	<div class="form-signin">
		<img class="center" id="profile-img" style="width:80%; text-align: center;" alt="Avatar" src=""""),_display_(/*24.98*/routes/*24.104*/.Assets.versioned("images/logo.png")),format.raw/*24.140*/("""" />
		<div class="title-connexion">
			<h3 class="text-center">
				<div class="big-title">Investir dans l'autonomisation des femmes.</div>
			</h3>
		</div>
		"""),_display_(/*30.4*/if(flash.containsKey("error"))/*30.34*/ {_display_(Seq[Any](format.raw/*30.36*/("""
		"""),format.raw/*31.3*/("""<div id="feedBackArea" style="margin-bottom: 6px !important;" class="alert alert-danger"> """),_display_(/*31.94*/flash/*31.99*/.get("error")),format.raw/*31.112*/("""</div>
		""")))}),format.raw/*32.4*/("""
		"""),_display_(/*33.4*/if(flash.containsKey("warning"))/*33.36*/ {_display_(Seq[Any](format.raw/*33.38*/("""
		"""),format.raw/*34.3*/("""<div id="feedBackArea" style="margin-bottom: 6px !important;" class="alert alert-warning">"""),_display_(/*34.94*/flash/*34.99*/.get("warning")),format.raw/*34.114*/("""</div>
		""")))}),format.raw/*35.4*/("""
		"""),_display_(/*36.4*/if(flash.containsKey("success"))/*36.36*/ {_display_(Seq[Any](format.raw/*36.38*/("""
		"""),format.raw/*37.3*/("""<div id="feedBackArea" style="margin-bottom: 6px !important;" class="alert alert-success">"""),_display_(/*37.94*/flash/*37.99*/.get("success")),format.raw/*37.114*/("""</div>
		""")))}),format.raw/*38.4*/("""
	"""),format.raw/*39.2*/("""</div>
	<form action=""""),_display_(/*40.17*/routes/*40.23*/.LoginController.authentification()),format.raw/*40.58*/("""" method="POST" class="form-signin">
		"""),_display_(/*41.4*/helper/*41.10*/.CSRF.formField),format.raw/*41.25*/("""
		"""),format.raw/*42.3*/("""<h2 class="form-signin-heading center">E-FUNGMATA</h2>
		<input type="email" class="form-control" name="email" placeholder="Adresse email" required="" autofocus="" />
		<br>
		<input type="password" class="form-control" name="password" placeholder="Mot de passe" required=""/>

		<button class="btn btn btn-primary btn-block" type="submit">Connexion</button>
		<a href=""""),_display_(/*48.13*/routes/*48.19*/.ApplicationUserCtrl.show("CREATE", 0L)),format.raw/*48.58*/("""" class="btn btn btn-success btn-block" >Inscrivez-vous</a>
	</form>
</div>
<div class=" navbar-fixed-bottom no-print" style="line-height: 23px; line-width:100%;">
	<div class="navbar-inner" style="min-height: 5px;">
                <span class="brand"><a style="margin-left: 3px;
                    color: red;
                    font-size: 13px;
                    text-shadow: 1px 1px 0 #fff;" href="#" title="techonoligie">KACKATHON</a></span>
	</div>
</div>
</body>
</html>
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 11:56:55 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/login.scala.html
                  HASH: aaa6f9e2acc65edab3a11ae1357a49e253a63a67
                  MATRIX: 941->1|1037->3|1065->5|1319->233|1333->239|1391->277|1452->311|1467->317|1522->351|1586->388|1615->389|1645->392|1745->465|1773->466|1801->467|1998->637|2014->643|2072->679|2260->841|2299->871|2339->873|2369->876|2487->967|2501->972|2536->985|2576->995|2606->999|2647->1031|2687->1033|2717->1036|2835->1127|2849->1132|2886->1147|2926->1157|2956->1161|2997->1193|3037->1195|3067->1198|3185->1289|3199->1294|3236->1309|3276->1319|3305->1321|3355->1344|3370->1350|3426->1385|3492->1425|3507->1431|3543->1446|3573->1449|3971->1820|3986->1826|4046->1865
                  LINES: 28->1|33->1|35->3|41->9|41->9|41->9|42->10|42->10|42->10|45->13|45->13|46->14|50->18|50->18|51->19|56->24|56->24|56->24|62->30|62->30|62->30|63->31|63->31|63->31|63->31|64->32|65->33|65->33|65->33|66->34|66->34|66->34|66->34|67->35|68->36|68->36|68->36|69->37|69->37|69->37|69->37|70->38|71->39|72->40|72->40|72->40|73->41|73->41|73->41|74->42|80->48|80->48|80->48
                  -- GENERATED --
              */
          