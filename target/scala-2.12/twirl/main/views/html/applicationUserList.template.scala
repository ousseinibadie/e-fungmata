
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object applicationUserList extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[commons.Pagination[models.tables.pojos.ApplicationUser],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(utilisateurs:commons.Pagination[models.tables.pojos.ApplicationUser], currentFilter : String, profil : String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

def /*4.2*/link/*4.6*/(newPage:Int) = {{
		    // Generate the link
		    controllers.routes.ApplicationUserCtrl.list(newPage,currentFilter)
		}};
Seq[Any](format.raw/*1.113*/("""


"""),format.raw/*7.4*/("""
"""),_display_(/*8.2*/main("Application User")/*8.26*/ {_display_(Seq[Any](format.raw/*8.28*/("""
"""),format.raw/*9.1*/("""<div style="margin-right: -80px;margin-left: 24px;">
	<div class="container">
        <div class="row">
    <label class="label label-info" style="float:right;">"""),_display_(/*12.59*/profil),format.raw/*12.65*/("""</label>
	<div class="badged-title">Liste des Utilisateurs</div>
			<br>
    <!-- @svNavTabsAdmin("USERS")-->
     <div class="panel-body">
		 <div class="tab-content">
			<div class="tab-pane fade in active" id="#tabUser">
                <form action=""""),_display_(/*19.32*/link(0)),format.raw/*19.39*/("""" method="GET">
                   <div class="form-inline">
	                     <a href=""""),_display_(/*21.33*/routes/*21.39*/.ApplicationUserCtrl.show("CREATE", 0L)),format.raw/*21.78*/("""" class="btn btn-info" data-rel="tooltip" title="Créer">
	                           <i class="fa fa-plus"></i>
	                      </a>
	                   <input id="searchbox" class="form-control" autofocus="" name="filter" value="" placeholder="nom utilisateur et email" style="width: 30%;" type="text">
	                   <button type="submit" class="btn btn-info " data-rel="tooltip" title="Rechercher/rafraichir">
	                                 <i class="fa fa-search"></i>
	                   </button>
	                  
                   </div>
                </form>

                <div class="row">
  			             <div class="panel panel-default">
  			                <table class="table table-bordered table-striped table-condensed">
    			          <thead>
						  	<tr class="breadcrumb">
	    				  		<th  style="text-align: center"><small>Nom</small></th>
								<th  style="text-align: center"><small>Prénom</small></th>
	    				   		<th  style="text-align: center">Email</th>
	    				   		<th  style="text-align: center">IS_Active</th>
	    				  		<th  style="text-align: center">Profil</th>
	    				  		<th  style="text-align: center">Modifier</th>

    				         </tr>
    			         </thead>
				         <tbody>    			
    			      """),_display_(/*47.15*/for(utilisateur <- utilisateurs.getList) yield /*47.55*/{_display_(Seq[Any](format.raw/*47.56*/("""
						 """),format.raw/*48.8*/("""<tr>
	    					<td width="15%">"""),_display_(/*49.28*/utilisateur/*49.39*/.getNom),format.raw/*49.46*/("""</td>
							 <td width="15%">"""),_display_(/*50.26*/utilisateur/*50.37*/.getPrenom),format.raw/*50.47*/("""</td>
	    					<td width="15%">"""),_display_(/*51.28*/utilisateur/*51.39*/.getEmail),format.raw/*51.48*/("""</td>
	    					<td width="5%">"""),_display_(/*52.27*/utilisateur/*52.38*/.getIsActive),format.raw/*52.50*/("""</td>
	    					<td width="5%">"""),_display_(/*53.27*/utilisateur/*53.38*/.getIsAdmin),format.raw/*53.49*/("""</td>
	    					<td width="32%">"""),_display_(/*54.28*/if(null != utilisateur.getMetaData && utilisateur.getMetaData.equals("admin"))/*54.106*/{_display_(Seq[Any](format.raw/*54.107*/("""admin""")))}/*54.113*/else/*54.117*/{_display_(_display_(/*54.119*/utilisateur/*54.130*/.getMetaData))}),format.raw/*54.143*/("""</td>
	    					<td style="text-align: center" width="6%">
	    						<a id="btnReadWrite" class="btn btn-success" data-rel="tooltip" title="Modifier" href=""""),_display_(/*56.100*/routes/*56.106*/.ApplicationUserCtrl.show("EDIT",utilisateur.getId)),format.raw/*56.157*/("""" type="button">
				                                           <i class="fa fa-edit"></i></a> 
				             </td>

              			</tr>
    					""")))}),format.raw/*61.11*/("""
    					
    				
   				"""),format.raw/*64.8*/("""</tbody>
  			</table>
  		</div>
                </div>
			            <ul class="pagination">
			                """),_display_(/*69.21*/if(utilisateurs.hasPrev)/*69.45*/ {_display_(Seq[Any](format.raw/*69.47*/("""
			                    """),format.raw/*70.24*/("""<li class="prev">
			                    <a href=""""),_display_(/*71.34*/link(utilisateurs.getPageIndex -1)),format.raw/*71.68*/("""">&larr; Precedent</a>
			                    </li>
			                """)))}/*73.22*/else/*73.27*/{_display_(Seq[Any](format.raw/*73.28*/("""
			                    """),format.raw/*74.24*/("""<li class="prev disabled">
			                        <a>&larr; Precedent</a>
			                    </li>
			                """)))}),format.raw/*77.21*/("""
			                """),format.raw/*78.20*/("""<li class="current">
			                    <a>utilisateurs """),_display_(/*79.41*/utilisateurs/*79.53*/.getDisplayXtoYofZ),format.raw/*79.71*/("""</a>  
			                </li>
			                """),_display_(/*81.21*/if(utilisateurs.hasNext())/*81.47*/ {_display_(Seq[Any](format.raw/*81.49*/("""
			                    """),format.raw/*82.24*/("""<li class="next">
			                    <a href=""""),_display_(/*83.34*/link(utilisateurs.getPageIndex+1)),format.raw/*83.67*/(""""> Suivant &rarr;</a>
			                    </li>
			                """)))}/*85.22*/else/*85.27*/{_display_(Seq[Any](format.raw/*85.28*/("""
			                    """),format.raw/*86.24*/("""<li class="next disabled">
			                        <a>Suivant &rarr;</a>
			                    </li>
			                """)))}),format.raw/*89.21*/("""
			            """),format.raw/*90.16*/("""</ul>
			    </div>
				</div>
			</div>

</div>
</div>
</div>
""")))}),format.raw/*98.2*/("""
"""))
      }
    }
  }

  def render(utilisateurs:commons.Pagination[models.tables.pojos.ApplicationUser],currentFilter:String,profil:String): play.twirl.api.HtmlFormat.Appendable = apply(utilisateurs,currentFilter,profil)

  def f:((commons.Pagination[models.tables.pojos.ApplicationUser],String,String) => play.twirl.api.HtmlFormat.Appendable) = (utilisateurs,currentFilter,profil) => apply(utilisateurs,currentFilter,profil)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 10:28:44 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/applicationUserList.scala.html
                  HASH: 4418d1171c72134d70ee6df87a97c5ac542ff8ed
                  MATRIX: 1025->1|1214->116|1225->120|1378->112|1407->242|1434->244|1466->268|1505->270|1532->271|1721->433|1748->439|2030->695|2058->702|2178->795|2193->801|2253->840|3562->2122|3618->2162|3657->2163|3692->2171|3751->2203|3771->2214|3799->2221|3857->2252|3877->2263|3908->2273|3968->2306|3988->2317|4018->2326|4077->2358|4097->2369|4130->2381|4189->2413|4209->2424|4241->2435|4301->2468|4389->2546|4429->2547|4455->2553|4469->2557|4500->2559|4521->2570|4558->2583|4744->2741|4760->2747|4833->2798|5017->2951|5071->2978|5214->3094|5247->3118|5287->3120|5339->3144|5417->3195|5472->3229|5563->3302|5576->3307|5615->3308|5667->3332|5825->3459|5873->3479|5961->3540|5982->3552|6021->3570|6100->3622|6135->3648|6175->3650|6227->3674|6305->3725|6359->3758|6449->3830|6462->3835|6501->3836|6553->3860|6709->3985|6753->4001|6847->4065
                  LINES: 28->1|32->4|32->4|36->1|39->7|40->8|40->8|40->8|41->9|44->12|44->12|51->19|51->19|53->21|53->21|53->21|79->47|79->47|79->47|80->48|81->49|81->49|81->49|82->50|82->50|82->50|83->51|83->51|83->51|84->52|84->52|84->52|85->53|85->53|85->53|86->54|86->54|86->54|86->54|86->54|86->54|86->54|86->54|88->56|88->56|88->56|93->61|96->64|101->69|101->69|101->69|102->70|103->71|103->71|105->73|105->73|105->73|106->74|109->77|110->78|111->79|111->79|111->79|113->81|113->81|113->81|114->82|115->83|115->83|117->85|117->85|117->85|118->86|121->89|122->90|130->98
                  -- GENERATED --
              */
          