
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object main extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import controllers.util.Helpers.getCurrentUserEmail
/*4.2*/import controllers.util.Helpers.getCurrentUser


Seq[Any](format.raw/*1.32*/("""

"""),format.raw/*5.1*/("""<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>"""),_display_(/*10.13*/title),format.raw/*10.18*/("""</title>

    <link rel="stylesheet" href='"""),_display_(/*12.35*/routes/*12.41*/.Assets.versioned("vendors/iconfonts/mdi/font/css/materialdesignicons.min.css")),format.raw/*12.120*/("""'>
    <link rel="stylesheet" href='"""),_display_(/*13.35*/routes/*13.41*/.Assets.versioned("vendors/css/vendor.bundle.base.css")),format.raw/*13.96*/("""'>
    <link rel="stylesheet" href='"""),_display_(/*14.35*/routes/*14.41*/.Assets.versioned("vendors/css/vendor.bundle.addons.css")),format.raw/*14.98*/("""'>

    <link rel="stylesheet" href='"""),_display_(/*16.35*/routes/*16.41*/.Assets.versioned("css/bootstrap.css")),format.raw/*16.79*/("""'>
    <link rel="stylesheet" href='"""),_display_(/*17.35*/routes/*17.41*/.Assets.versioned("web-fonts-with-css/css/fontawesome-all.css")),format.raw/*17.104*/("""'>
    <link rel="stylesheet" href='"""),_display_(/*18.35*/routes/*18.41*/.Assets.versioned("css/jquery-ui.min.css")),format.raw/*18.83*/("""'>
    <link rel="stylesheet" href='"""),_display_(/*19.35*/routes/*19.41*/.Assets.versioned("css/style.css")),format.raw/*19.75*/("""'>
    <link rel="stylesheet" href='"""),_display_(/*20.35*/routes/*20.41*/.Assets.versioned("css/sp-main.css")),format.raw/*20.77*/("""'>

    <link rel="stylesheet" type="text/css" media="screen, print" href=""""),_display_(/*22.73*/routes/*22.79*/.Assets.versioned("stylesheets/AdminLTE.min.css")),format.raw/*22.128*/("""">

    <link rel="shortcut icon" href='"""),_display_(/*24.38*/routes/*24.44*/.Assets.versioned("images/logo.png")),format.raw/*24.80*/("""'/>
    <style>

        div.box """),format.raw/*27.17*/("""{"""),format.raw/*27.18*/("""
    """),format.raw/*28.5*/("""background-color: #fff;
    border: 1px solid #cccccc;
    border-radius: 5px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
    margin: 4px 4px 0;
    padding: 5px;
    text-align: left;
"""),format.raw/*35.1*/("""}"""),format.raw/*35.2*/("""
    """),format.raw/*36.5*/("""</style>

</head>
<body>
<div class="container-scroller">
    <!-- partial:partials/_horizontal-navbar.html -->
    <div class="horizontal-menu">
        <nav class="navbar top-navbar col-lg-12 col-12 p-0">
            <div class="navbar-menu-wrapper d-flex align-items-stretch justify-content-between">
                <ul class="navbar-nav mr-lg-2 d-none d-lg-flex">

                    <!--
                                                <img src='"""),_display_(/*48.60*/routes/*48.66*/.Assets.versioned("images/armoirie_d_niger.png")),format.raw/*48.114*/("""' style="width:100px; height:50px" alt="logo"/></a>
                    -->

                    <li class="nav-item nav-search d-none d-lg-flex">
                        <a class="navbar-brand" href="/" >
                            <i style="color: #ffffff; font-size: 25px; text-shadow: none;">E-FUNDMATA</i>
                        </a>
                    </li>
                </ul>


                <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">

                    <a class="navbar-brand brand-logo-mini" href=""><img src='"""),_display_(/*61.80*/routes/*61.86*/.Assets.versioned("images/armoirie_d_niger.png")),format.raw/*61.134*/("""' alt="logo"/></a>
                </div>
                """),_display_(/*63.18*/if(getCurrentUserEmail != null)/*63.49*/{_display_(Seq[Any](format.raw/*63.50*/("""
                    """),format.raw/*64.21*/("""<ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                                <i class="mdi mdi-bell-outline mx-0"></i>
                                <span class="count"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                                <a class="dropdown-item">
                                    <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
                                    </p>
                                    <span class="badge badge-pill badge-warning float-right">View all</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-success">
                                            <i class="mdi mdi-information mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-medium">Application Error</h6>
                                        <p class="font-weight-light small-text mb-0">
                                            Just now
                                        </p>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-warning">
                                            <i class="mdi mdi-settings mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-medium">Settings</h6>
                                        <p class="font-weight-light small-text mb-0">
                                            Private message
                                        </p>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-info">
                                            <i class="mdi mdi-account-box mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-medium">New user registration</h6>
                                        <p class="font-weight-light small-text mb-0">
                                            2 days ago
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                                <img src=""""),_display_(/*122.44*/routes/*122.50*/.Assets.versioned("images/face5.jpg")),format.raw/*122.87*/("""" alt="profile"/>
                                <span class="nav-profile-name">"""),_display_(/*123.65*/getCurrentUser()/*123.81*/.getEmail),format.raw/*123.90*/("""</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                                <a class="dropdown-item" href=""""),_display_(/*126.65*/routes/*126.71*/.ApplicationUserCtrl.show("EDIT", getCurrentUser().getId)),format.raw/*126.128*/("""" >
                                    <i class="mdi mdi-settings text-primary"></i>
                                    Compte
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/logout">
                                    <i class="mdi mdi-logout text-primary"></i>
                                    Se deconnecter
                                </a>
                            </div>
                        </li>
                        <li class="nav-item nav-toggler-item-right d-lg-none">
                            <button class="navbar-toggler align-self-center" type="button" data-toggle="horizontal-menu-toggle">
                                <span class="mdi mdi-menu"></span>
                            </button>
                        </li>
                    </ul>
                """)))}/*143.18*/else/*143.22*/{_display_(Seq[Any](format.raw/*143.23*/("""
                    """),format.raw/*144.21*/("""<ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item nav-profile dropdown">
                            <a class="btn dropdown-item btn-warning" href=""""),_display_(/*146.77*/routes/*146.83*/.LoginController.login()),format.raw/*146.107*/("""" >Connexion <i class="fa fa-power-on"></i></a>
                        </li>
                    </ul>
                """)))}),format.raw/*149.18*/("""
                """),format.raw/*150.17*/("""</div>

            </nav>

        <nav class="bottom-navbar">
            <div class="container">
                <ul class="nav page-navigation">
                    <li class="nav-item">
                        <a class="nav-link" href="/">
                            <i class="mdi mdi-view-dashboard-outline menu-icon"></i>
                            <span class="menu-title">Accueil</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=""""),_display_(/*164.52*/routes/*164.58*/.ProjetAttenteCtrl.show("CREATE",0L)),format.raw/*164.94*/("""">
                            <i class="mdi mdi-airplay menu-icon"></i>
                            <span class="menu-title">Soumettre projets </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=""""),_display_(/*170.52*/routes/*170.58*/.ProjetAttenteCtrl.projetInvestissement()),format.raw/*170.99*/("""">
                            <i class="mdi mdi-file-document-box-outline menu-icon"></i>
                            <span class="menu-title">Projets soumis</span>
                        </a>
                    </li>

                    <!--<li class="nav-item mega-menu">
                        <a href="#" class="nav-link">
                            <i class="mdi mdi-puzzle-outline menu-icon"></i>
                            <span class="menu-title">UI Elements</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="submenu">
                            <div class="col-group-wrapper row">
                                <div class="col-group col-md-4">
                                    <div class="row">
                                        <div class="col-12">
                                            <p class="category-heading">Basic Elements</p>
                                            <div class="submenu-item">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/accordions.html">Accordion</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/badges.html">Badges</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/breadcrumbs.html">Breadcrumbs</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/dropdowns.html">Dropdown</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/modals.html">Modals</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/progress.html">Progress bar</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/pagination.html">Pagination</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/tabs.html">Tabs</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/tooltips.html">Tooltip</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-group col-md-4">
                                    <div class="row">
                                        <div class="col-12">
                                            <p class="category-heading">Advanced Elements</p>
                                            <div class="submenu-item">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/dragula.html">Dragula</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/carousel.html">Carousel</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/clipboard.html">Clipboard</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/context-menu.html">Context Menu</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/loaders.html">Loader</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/slider.html">Slider</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/popups.html">Popup</a></li>
                                                            <li class="nav-item"><a class="nav-link" href="pages/ui-features/notifications.html">Notification</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-group col-md-4">
                                    <p class="category-heading">Icons</p>
                                    <ul class="submenu-item">
                                        <li class="nav-item"><a class="nav-link" href="pages/icons/flag-icons.html">Flag Icons</a></li>
                                        <li class="nav-item"><a class="nav-link" href="pages/icons/font-awesome.html">Font Awesome</a></li>
                                        <li class="nav-item"><a class="nav-link" href="pages/icons/simple-line-icon.html">Simple Line Icons</a></li>
                                        <li class="nav-item"><a class="nav-link" href="pages/icons/themify.html">Themify Icons</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>-->
                    <!--<li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="mdi mdi-file-document-box-outline menu-icon"></i>
                            <span class="menu-title">Forms</span>
                            <i class="menu-arrow"></i></a>
                        <div class="submenu">
                            <ul class="submenu-item">
                                <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">Basic Elements</a></li>
                                <li class="nav-item"><a class="nav-link" href="pages/forms/advanced_elements.html">Advanced Elements</a></li>
                                <li class="nav-item"><a class="nav-link" href="pages/forms/validation.html">Validation</a></li>
                                <li class="nav-item"><a class="nav-link" href="pages/forms/wizard.html">Wizard</a></li>
                                <li class="nav-item"><a class="nav-link" href="pages/forms/text_editor.html">Text Editor</a></li>
                                <li class="nav-item"><a class="nav-link" href="pages/forms/code_editor.html">Code Editor</a></li>
                            </ul>
                        </div>
                    </li>
-->

                    <li class="nav-item">
                        <a href=""""),_display_(/*272.35*/routes/*272.41*/.ApplicationUserCtrl.show("CREATE", 0L)),format.raw/*272.80*/("""" class="nav-link">
                            <i class="mdi mdi-file-document-box-outline menu-icon"></i>
                            <span class="menu-title">S'inscrire</span></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <div class="main-panel">
            <br>
            <div class="container">
                """),_display_(/*285.18*/if(flash.containsKey("error"))/*285.48*/ {_display_(Seq[Any](format.raw/*285.50*/("""
                    """),format.raw/*286.21*/("""<div id="feedBackArea" style="margin-bottom: 6px !important;" class="alert alert-danger"> """),_display_(/*286.112*/flash/*286.117*/.get("error")),format.raw/*286.130*/("""</div>
                """)))}),format.raw/*287.18*/("""
                """),_display_(/*288.18*/if(flash.containsKey("warning"))/*288.50*/ {_display_(Seq[Any](format.raw/*288.52*/("""
                    """),format.raw/*289.21*/("""<div id="feedBackArea" style="margin-bottom: 6px !important;" class="alert alert-warning">"""),_display_(/*289.112*/flash/*289.117*/.get("warning")),format.raw/*289.132*/("""</div>
                """)))}),format.raw/*290.18*/("""
                """),_display_(/*291.18*/if(flash.containsKey("success"))/*291.50*/ {_display_(Seq[Any](format.raw/*291.52*/("""
                    """),format.raw/*292.21*/("""<div id="feedBackArea" style="margin-bottom: 6px !important;" class="alert alert-success">"""),_display_(/*292.112*/flash/*292.117*/.get("success")),format.raw/*292.132*/("""</div>
                """)))}),format.raw/*293.18*/("""
                """),_display_(/*294.18*/content),format.raw/*294.25*/("""
            """),format.raw/*295.13*/("""</div>
        </div>
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->

    <!-- partial -->
</div>
<div class=" navbar-fixed-bottom no-print" style="line-height: 14px; line-width:100%;">
    <div class="navbar-inner" style="min-height: 5px;">
                <span class="brand"><a style="margin-left: 3px;
                    color: red;
                    font-size: 13px;
                    text-shadow: 1px 1px 0 #fff;" href="http://advinteck.com" title="advinteck techonoligie">HACKATHON 2019</a></span>
    </div>
</div>



<script src='"""),_display_(/*314.15*/routes/*314.21*/.Assets.versioned("js/off-canvas.js")),format.raw/*314.58*/("""'></script>
<script src='"""),_display_(/*315.15*/routes/*315.21*/.Assets.versioned("js/hoverable-collapse.js")),format.raw/*315.66*/("""'></script>
<script src='"""),_display_(/*316.15*/routes/*316.21*/.Assets.versioned("js/template.js")),format.raw/*316.56*/("""'></script>
<script src='"""),_display_(/*317.15*/routes/*317.21*/.Assets.versioned("js/settings.js")),format.raw/*317.56*/("""'></script>
<script src='"""),_display_(/*318.15*/routes/*318.21*/.Assets.versioned("js/todolist.js")),format.raw/*318.56*/("""'></script>
<script src='"""),_display_(/*319.15*/routes/*319.21*/.Assets.versioned("js/dashboard.js")),format.raw/*319.57*/("""'></script>
<script src='"""),_display_(/*320.15*/routes/*320.21*/.Assets.versioned("js/todolist.js")),format.raw/*320.56*/("""'></script>
<script src='"""),_display_(/*321.15*/routes/*321.21*/.Assets.versioned("js/vendor.bundle.base.js")),format.raw/*321.66*/("""'></script>
<script src='"""),_display_(/*322.15*/routes/*322.21*/.Assets.versioned("js/vendor.bundle.addons.js")),format.raw/*322.68*/("""'></script>

<script src='"""),_display_(/*324.15*/routes/*324.21*/.Assets.versioned("js/jquery-3.3.1.js")),format.raw/*324.60*/("""'></script>
<script src='"""),_display_(/*325.15*/routes/*325.21*/.Assets.versioned("js/jquery.min.js")),format.raw/*325.58*/("""'></script>
<script src='"""),_display_(/*326.15*/routes/*326.21*/.Assets.versioned("javascripts/jquery.multi-select.js")),format.raw/*326.76*/("""' type='text/javascript'></script>

<!-- <script src='"""),_display_(/*328.20*/routes/*328.26*/.Assets.versioned("js/jquery-ui.min.js")),format.raw/*328.66*/("""'></script> -->
<script src='"""),_display_(/*329.15*/routes/*329.21*/.Assets.versioned("js/jquery.number.min.js")),format.raw/*329.65*/("""' type='text/javascript'></script>
<script src='"""),_display_(/*330.15*/routes/*330.21*/.Assets.versioned("js/jquery.autocomplete.min.js")),format.raw/*330.71*/("""' type='text/javascript'></script>
<script src='"""),_display_(/*331.15*/routes/*331.21*/.Assets.versioned("js/lodash.min.js")),format.raw/*331.58*/("""' type='text/javascript'></script>
<script src='"""),_display_(/*332.15*/routes/*332.21*/.Assets.versioned("js/autoNumeric.min.js")),format.raw/*332.63*/("""' type='text/javascript'></script>

<script src='"""),_display_(/*334.15*/routes/*334.21*/.Assets.versioned("js/jquery.copycss.js")),format.raw/*334.62*/("""' type='text/javascript'></script>
<script src='"""),_display_(/*335.15*/routes/*335.21*/.Assets.versioned("js/jquery.print-preview.js")),format.raw/*335.68*/("""' type='text/javascript'></script>

<script src='"""),_display_(/*337.15*/routes/*337.21*/.Assets.versioned("js/bootstrap.min.js")),format.raw/*337.61*/("""'></script>
<script src='"""),_display_(/*338.15*/routes/*338.21*/.Assets.versioned("js/script.js")),format.raw/*338.54*/("""'></script>
<script src='"""),_display_(/*339.15*/routes/*339.21*/.Assets.versioned("js/main.js")),format.raw/*339.52*/("""'></script>
</body>
</html>"""))
      }
    }
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Apr 28 11:08:17 WAT 2019
                  SOURCE: /home/abdoul-razack/Bureau/test-projet/hachathon/e-griot/app/views/main.scala.html
                  HASH: 024352034676ee20f79b06ab44aff004d311600d
                  MATRIX: 952->1|1055->34|1114->87|1190->31|1218->134|1438->327|1464->332|1535->376|1550->382|1651->461|1715->498|1730->504|1806->559|1870->596|1885->602|1963->659|2028->697|2043->703|2102->741|2166->778|2181->784|2266->847|2330->884|2345->890|2408->932|2472->969|2487->975|2542->1009|2606->1046|2621->1052|2678->1088|2781->1164|2796->1170|2867->1219|2935->1260|2950->1266|3007->1302|3068->1335|3097->1336|3129->1341|3344->1529|3372->1530|3404->1535|3885->1989|3900->1995|3970->2043|4580->2626|4595->2632|4665->2680|4751->2739|4791->2770|4830->2771|4879->2792|8818->6703|8834->6709|8893->6746|9003->6828|9029->6844|9060->6853|9319->7084|9335->7090|9415->7147|10362->8074|10376->8078|10416->8079|10466->8100|10678->8284|10694->8290|10741->8314|10894->8435|10940->8452|11514->8998|11530->9004|11588->9040|11916->9340|11932->9346|11995->9387|20334->17698|20350->17704|20411->17743|20891->18195|20931->18225|20972->18227|21022->18248|21142->18339|21158->18344|21194->18357|21250->18381|21296->18399|21338->18431|21379->18433|21429->18454|21549->18545|21565->18550|21603->18565|21659->18589|21705->18607|21747->18639|21788->18641|21838->18662|21958->18753|21974->18758|22012->18773|22068->18797|22114->18815|22143->18822|22185->18835|22799->19421|22815->19427|22874->19464|22928->19490|22944->19496|23011->19541|23065->19567|23081->19573|23138->19608|23192->19634|23208->19640|23265->19675|23319->19701|23335->19707|23392->19742|23446->19768|23462->19774|23520->19810|23574->19836|23590->19842|23647->19877|23701->19903|23717->19909|23784->19954|23838->19980|23854->19986|23923->20033|23978->20060|23994->20066|24055->20105|24109->20131|24125->20137|24184->20174|24238->20200|24254->20206|24331->20261|24414->20316|24430->20322|24492->20362|24550->20392|24566->20398|24632->20442|24709->20491|24725->20497|24797->20547|24874->20596|24890->20602|24949->20639|25026->20688|25042->20694|25106->20736|25184->20786|25200->20792|25263->20833|25340->20882|25356->20888|25425->20935|25503->20985|25519->20991|25581->21031|25635->21057|25651->21063|25706->21096|25760->21122|25776->21128|25829->21159
                  LINES: 28->1|31->3|32->4|35->1|37->5|42->10|42->10|44->12|44->12|44->12|45->13|45->13|45->13|46->14|46->14|46->14|48->16|48->16|48->16|49->17|49->17|49->17|50->18|50->18|50->18|51->19|51->19|51->19|52->20|52->20|52->20|54->22|54->22|54->22|56->24|56->24|56->24|59->27|59->27|60->28|67->35|67->35|68->36|80->48|80->48|80->48|93->61|93->61|93->61|95->63|95->63|95->63|96->64|154->122|154->122|154->122|155->123|155->123|155->123|158->126|158->126|158->126|175->143|175->143|175->143|176->144|178->146|178->146|178->146|181->149|182->150|196->164|196->164|196->164|202->170|202->170|202->170|304->272|304->272|304->272|317->285|317->285|317->285|318->286|318->286|318->286|318->286|319->287|320->288|320->288|320->288|321->289|321->289|321->289|321->289|322->290|323->291|323->291|323->291|324->292|324->292|324->292|324->292|325->293|326->294|326->294|327->295|346->314|346->314|346->314|347->315|347->315|347->315|348->316|348->316|348->316|349->317|349->317|349->317|350->318|350->318|350->318|351->319|351->319|351->319|352->320|352->320|352->320|353->321|353->321|353->321|354->322|354->322|354->322|356->324|356->324|356->324|357->325|357->325|357->325|358->326|358->326|358->326|360->328|360->328|360->328|361->329|361->329|361->329|362->330|362->330|362->330|363->331|363->331|363->331|364->332|364->332|364->332|366->334|366->334|366->334|367->335|367->335|367->335|369->337|369->337|369->337|370->338|370->338|370->338|371->339|371->339|371->339
                  -- GENERATED --
              */
          